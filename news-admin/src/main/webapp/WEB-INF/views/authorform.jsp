<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<tiles:insertDefinition name="defaultTemplate">
	<tiles:putAttribute name="body">
		<div class="body">
			<c:url value="/admin/processauthor" var="processAuthor" />
			<form:form name="edit" action="${processAuthor}" method="post"
				modelAttribute="author">
				<input type="hidden" name="${_csrf.parameterName}"
					value="${_csrf.token}" />
				<form:hidden path="authorId" />
				<form:errors path="authorName" class="error" />
				<c:forEach var="author" items="${authorList}">
					<c:if test="${author.expired == null}">
						<table class="add-author">
							<tbody>
								<tr>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
								</tr>
								<tr>
									<td><form:label path="authorName">
											<b><spring:message code="author.author" /></b>
										</form:label></td>
									<td><c:choose>
											<c:when test="${editAuthorId eq author.authorId}">
												<form:textarea path="authorName" rows="1" cols="40"
													maxlength="30"></form:textarea>
											</c:when>
											<c:otherwise>
												<textarea disabled="disabled" rows="1" cols="40"><c:out
														value="${author.authorName}" /></textarea>
											</c:otherwise>
										</c:choose></td>
									<td><c:choose>
											<c:when test="${editAuthorId eq author.authorId}">
												<a href="javascript:document.edit.submit();"><spring:message
														code="update.author" /></a>
												<a
													href="<c:url value="/admin/addauthor?authorId=${author.authorId}&authorName=${author.authorName}"/>"><spring:message
														code="expire.author" /></a>
												<a href="<c:url value="/admin/addauthor"/>"><spring:message
														code="cancel.author" /></a>
											</c:when>
											<c:otherwise>
												<a
													href="<c:url value="/admin/editauthor/${author.authorId}?authorName=${author.authorName}"/>"><spring:message
														code="edit.author" /></a>
											</c:otherwise>
										</c:choose></td>
								</tr>
							</tbody>
						</table>
					</c:if>
				</c:forEach>

				<table class="add-author">
					<tbody>
						<tr>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td><form:label path="authorName">
									<b><spring:message code="add.author" /></b>
								</form:label></td>
							<td><c:choose>
									<c:when test="${editAuthorId == null}">
										<form:textarea path="authorName" rows="1" cols="40"
											maxlength="30"></form:textarea>
									</c:when>
									<c:otherwise>
										<textarea disabled="disabled" rows="1" cols="40"></textarea>
									</c:otherwise>
								</c:choose></td>
							<td><a
								href="javascript: if(validateAuthorForm()){document.edit.submit();}"><spring:message
										code="save.author" /></a></td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
					</tbody>
				</table>

			</form:form>
		</div>
	</tiles:putAttribute>
</tiles:insertDefinition>
