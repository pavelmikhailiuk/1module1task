<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="menu-div">
	<script>
		$(function() {
			$('a').each(function() {
				if ($(this).prop('href') == window.location.href) {
					$(this).addClass('current');
				}
			});
		});
	</script>
	<table class="border">
		<tr>
			<td>
				<table class="menu">
					<tr>
						<td><spring:message code="menu.symbol" /> <a
							href="<c:url value="/admin/listnews"/>"><spring:message code="list.news" /></a></td>
					</tr>
					<tr>
						<td><spring:message code="menu.symbol" /> <a
							href="<c:url value="/admin/addnews"/>"><spring:message code="add.news" /></a></td>
					</tr>
					<tr>
						<td><spring:message code="menu.symbol" /> <a
							href="<c:url value="/admin/addauthor"/>"><spring:message
									code="add.authors" /></a></td>
					</tr>
					<tr>
						<td><spring:message code="menu.symbol" /> <a
							href="<c:url value="/admin/addtag"/>"><spring:message code="add.tags" /></a></td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</div>
