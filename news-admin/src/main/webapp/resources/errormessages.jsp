<%@page language="java" contentType="text/javascript; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<fmt:setBundle basename="webResources" />
<fmt:setLocale value="${cookie.localeCookie.value}" scope="session"/>
var msg="<fmt:message key="error.comment" />";
var tagAndAuthorMsg="<fmt:message key="error.field" />";
var newsMsg="<fmt:message key="error.all.fields" />";
