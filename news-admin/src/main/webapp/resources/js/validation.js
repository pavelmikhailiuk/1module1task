function validateNewsForm() {
	var nt = document.forms["newsForm"]["news.title"].value;
	var ns = document.forms["newsForm"]["news.shortText"].value;
	var nf = document.forms["newsForm"]["news.fullText"].value;
	if (nt == null || nt == "" || ns == null || ns == "" || nf == null
			|| nf == "") {
		alert(newsMsg);
		return false;
	}
	return true;
}

function validateTagForm() {
	var tg = document.forms["edit"]["tagName"].value;
	return validate(tg);
}

function validateAuthorForm() {
	var at = document.forms["edit"]["authorName"].value;
	return validate(at);
}

function validateCommentForm() {
	var ct = document.forms["commentForm"]["commentText"].value;
	if (ct == null || ct == "") {
		alert(msg);
		return false;
	}
	return true;
}

function validate(param) {
	if (param == null || param == "") {
		alert(tagAndAuthorMsg);
		return false;
	}
	return true;
}