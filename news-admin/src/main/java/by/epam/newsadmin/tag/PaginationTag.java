package by.epam.newsadmin.tag;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PaginationTag extends TagSupport {

	private static final long serialVersionUID = 1L;

	private static final String LINK_TO_PAGE = "/admin/listnews?pageNumber=";

	private static final String FORM_ACTION = "newsadmin/admin/listnews";

	private final static Logger LOGGER = LoggerFactory.getLogger(PaginationTag.class);

	private int noOfPages;

	private int currentPage;

	public PaginationTag() {
	}

	public PaginationTag(int noOfPages, int currentPage) {
		this.noOfPages = noOfPages;
		this.currentPage = currentPage;
	}

	public void setNoOfPages(int noOfPages) {
		this.noOfPages = noOfPages;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.servlet.jsp.tagext.TagSupport#doStartTag()
	 */
	@Override
	public int doStartTag() throws JspException {
		try (JspWriter out = pageContext.getOut()) {
			out.write(createAllLinks());
		} catch (IOException e) {
			LOGGER.error("Custom tag error", e);
		}
		return SKIP_BODY;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.servlet.jsp.tagext.TagSupport#doEndTag()
	 */
	@Override
	public int doEndTag() throws JspException {
		return EVAL_PAGE;
	}

	/**
	 * Creates the all links.
	 *
	 * @return the string
	 */
	private String createAllLinks() {
		StringBuilder sb = new StringBuilder();
		int prevPage = currentPage - 1 > 0 ? currentPage - 1 : 1;
		int nextPage = currentPage + 1 <= noOfPages ? currentPage + 1 : noOfPages;
		createLink(sb, 1, "<<<");
		createLink(sb, prevPage, "<");
		createPageNumberInputForm(sb, noOfPages, currentPage);
		createLink(sb, nextPage, ">");
		createLink(sb, noOfPages, ">>>");
		return sb.toString();
	}

	/**
	 * Creates the link.
	 *
	 * @param sb
	 *            the StringBuilder
	 * @param pageNumber
	 *            the page number
	 * @param direction
	 *            the direction of view
	 */
	private void createLink(StringBuilder sb, int pageNumber, String direction) {
		HttpServletRequest request = (HttpServletRequest) pageContext.getRequest();
		sb.append("<td style='padding-right:15px'><a href='").append(request.getContextPath()).append(LINK_TO_PAGE)
				.append(pageNumber).append("'>").append(direction).append("</a></td>");
	}

	/**
	 * Creates the page number input form.
	 *
	 * @param sb
	 *            the StringBuilder
	 * @param noOfPages
	 *            the number of pages
	 * @param currentPage
	 *            the number of current page
	 */
	private void createPageNumberInputForm(StringBuilder sb, int noOfPages, int currentPage) {
		sb.append("<td style='padding-right:15px'><form action='").append(FORM_ACTION)
				.append("'><input type='number' name='pageNumber' min='1' max='").append(noOfPages)
				.append("' step='1' value='").append(currentPage).append("'></form></td>");
	}
}
