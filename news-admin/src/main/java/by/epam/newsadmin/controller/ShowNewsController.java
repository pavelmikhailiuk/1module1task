package by.epam.newsadmin.controller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import by.epam.newsadmin.service.NewsPaginationProcessor;
import by.epam.newscommon.exception.ServiceException;
import by.epam.newscommon.service.NewsManagementService;
import by.epam.newsdao.entity.Comment;
import by.epam.newsdao.entity.NewsDTO;

/**
 * The Class ShowNewsController.
 */
@Controller
public class ShowNewsController {

	/** The Constant NOT_VALID_COMMENT_TEXT. */
	private static final String NOT_VALID_COMMENT_TEXT = "notValidCommentText";

	/** The Constant REQUEST_ATTRIBUTE_ERROR. */
	private static final String MODEL_ATTRIBUTE_ERROR = "error";

	/** The Constant ADMIN. */
	private static final String ADMIN = "/admin/";

	/** The Constant VIEW_NAME_SHOWNEWS. */
	private static final String VIEW_NAME_SHOWNEWS = "shownews";

	/** The Constant VIEW_NAME_REDIRECT_ADMIN_SHOWNEWS. */
	private static final String VIEW_NAME_REDIRECT_ADMIN_SHOWNEWS = "redirect:/admin/shownews/";

	/** The Constant SESSION_ATTRIBUTE_OFFSET. */
	private static final String SESSION_ATTRIBUTE_OFFSET = "offSet";

	/** The Constant SESSION_ATTRIBUTE_NEWS_DTO_LIST. */
	private static final String SESSION_ATTRIBUTE_NEWS_DTO_LIST = "newsDTOList";

	/** The Constant SESSION_ATTRIBUTE_PREV_NEWS_DTO. */
	private static final String SESSION_ATTRIBUTE_PREV_NEWS_DTO = "prevNewsDTO";

	/** The Constant SESSION_ATTRIBUTE_NEXT_NEWS_DTO. */
	private static final String SESSION_ATTRIBUTE_NEXT_NEWS_DTO = "nextNewsDTO";

	/** The Constant MODEL_ATTRIBUTE_NEWS_DTO. */
	private static final String MODEL_ATTRIBUTE_NEWS_DTO = "newsDTO";

	/** The Constant NEWS_AT_PAGE. */
	public static final int NEWS_AT_PAGE = 1;

	/** The news management service. */
	@Autowired
	private NewsManagementService newsManagementService;

	/** The pagination processor. */
	@Autowired
	private NewsPaginationProcessor processor;

	/**
	 * Show news.
	 *
	 * @param newsId
	 *            the news id
	 * @param direction
	 *            the view direction
	 * @param model
	 *            the model and view object
	 * @param request
	 *            the request
	 * @param session
	 *            the session
	 * @return the string
	 * @throws ServiceException
	 *             the service exception if operation failed
	 */
	@RequestMapping(value = { ADMIN + "shownews/{newsId}" }, method = RequestMethod.GET)
	public String showNews(@PathVariable Long newsId, @RequestParam(defaultValue = "") String direction, Model model,
			HttpServletRequest request, HttpSession session) throws ServiceException {
		Integer offSet = processor.calculateOffSet(newsId, direction, session);
		NewsDTO newsDTO = newsManagementService.findNews(newsId);
		NewsDTO prevNewsDTO = findNewsDTO(request, offSet - 1);
		NewsDTO nextNewsDTO = findNewsDTO(request, offSet + 1);
		session.setAttribute(SESSION_ATTRIBUTE_OFFSET, offSet);
		model.addAttribute(MODEL_ATTRIBUTE_NEWS_DTO, newsDTO);
		session.setAttribute(SESSION_ATTRIBUTE_NEXT_NEWS_DTO, nextNewsDTO);
		session.setAttribute(SESSION_ATTRIBUTE_PREV_NEWS_DTO, prevNewsDTO);
		session.setAttribute(SESSION_ATTRIBUTE_NEWS_DTO_LIST, null);
		return VIEW_NAME_SHOWNEWS;
	}

	/**
	 * Adds the comment.
	 *
	 * @param newsId
	 *            the news id
	 * @param commentText
	 *            the comment text
	 * @param model
	 *            the model and view object
	 * @param session
	 *            the session
	 * @return the model and view object
	 * @throws ServiceException
	 *             the service exception if operation failed
	 */
	@RequestMapping(value = { ADMIN + "addcomment" }, method = RequestMethod.POST)
	public ModelAndView addComment(@RequestParam long newsId, @RequestParam String commentText, ModelAndView model,
			HttpSession session, RedirectAttributes redirAttr) throws ServiceException {
		if (commentText != null && !commentText.equals("")) {
			Comment comment = new Comment(1L, newsId, commentText, new Date());
			newsManagementService.saveComment(comment);
		} else {
			redirAttr.addFlashAttribute(MODEL_ATTRIBUTE_ERROR, NOT_VALID_COMMENT_TEXT);
		}
		model.setViewName(VIEW_NAME_REDIRECT_ADMIN_SHOWNEWS + newsId);
		return model;
	}

	/**
	 * Delete comment.
	 *
	 * @param newsId
	 *            the news id
	 * @param commentId
	 *            the comment id
	 * @param model
	 *            the model abnd view object
	 * @param session
	 *            the session
	 * @return the model and view object
	 * @throws ServiceException
	 *             the service exception if operation failed
	 */
	@RequestMapping(value = { ADMIN + "deletecomment" }, method = RequestMethod.POST)
	public ModelAndView deleteComment(@RequestParam long newsId, @RequestParam long commentId, ModelAndView model,
			HttpSession session) throws ServiceException {
		newsManagementService.deleteComment(commentId);
		model.setViewName(VIEW_NAME_REDIRECT_ADMIN_SHOWNEWS + newsId);
		return model;
	}

	/**
	 * Find news DTO.
	 *
	 * @param request
	 *            the request
	 * @param offSet
	 *            the offset
	 * @return the news DTO object
	 * @throws ServiceException
	 *             the service exception if operation failed
	 */
	private NewsDTO findNewsDTO(HttpServletRequest request, Integer offSet) throws ServiceException {
		List<NewsDTO> newsDTOList = processor.processNewsForPage(request, offSet, NEWS_AT_PAGE);
		return !newsDTOList.isEmpty() ? newsDTOList.get(0) : null;
	}
}
