package by.epam.newsadmin.controller;

import java.text.SimpleDateFormat;
import java.util.*;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomCollectionEditor;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import by.epam.newscommon.exception.ServiceException;
import by.epam.newscommon.service.NewsManagementService;
import by.epam.newsdao.entity.*;

/**
 * The Class NewsFormController.
 */
@Controller
public class NewsFormController {

	/** The Constant NEWS_DTO_FIELD_TAGS. */
	private static final String NEWS_DTO_FIELD_TAGS = "tags";

	/** The Constant ADMIN. */
	private static final String ADMIN = "/admin";

	/** The Constant MM_DD_YYYY. */
	private static final String MM_DD_YYYY = "MM/dd/yyyy";

	/** The Constant DD_MM_YYYY. */
	private static final String DD_MM_YYYY = "dd/MM/yyyy";

	/** The Constant MESSAGE_AUTHOR_HAVEN_T_BEEN_CHOSEN. */
	private static final String MESSAGE_AUTHOR_HAVEN_T_BEEN_CHOSEN = "author haven't been chosen";

	/** The Constant MODEL_ATTRIBUTE_SELECT_AUTHOR_MSG. */
	private static final String MODEL_ATTRIBUTE_SELECT_AUTHOR_MSG = "selectAuthorMsg";

	/** The Constant MODEL_ATTRIBUTE_TAG_LIST. */
	private static final String MODEL_ATTRIBUTE_TAG_LIST = "tagList";

	/** The Constant MODEL_ATTRIBUTE_AUTHOR_LIST. */
	private static final String MODEL_ATTRIBUTE_AUTHOR_LIST = "authorList";

	/** The Constant MODEL_ATTRIBUTE_NEWS_DTO. */
	private static final String MODEL_ATTRIBUTE_NEWS_DTO = "newsDTO";

	/** The Constant VIEW_NAME_NEWSFORM. */
	private static final String VIEW_NAME_NEWSFORM = "newsform";

	/** The Constant VIEW_NAME_SHOWNEWS. */
	private static final String VIEW_NAME_SHOWNEWS = "shownews";

	/** The news management service. */
	@Autowired
	private NewsManagementService newsManagementService;

	/**
	 * Adds the news.
	 *
	 * @param model
	 *            the model and view object
	 * @return the model and view object
	 * @throws ServiceException
	 *             the service exception if operation failed
	 */
	@RequestMapping(value = { ADMIN + "/addnews" })
	public ModelAndView addNews(ModelAndView model) throws ServiceException {
		News news = new News();
		news.setModificationDate(new Date());
		NewsDTO newsDTO = new NewsDTO();
		newsDTO.setNews(news);
		prepareFormView(model, null);
		model.addObject(MODEL_ATTRIBUTE_NEWS_DTO, newsDTO);
		model.setViewName(VIEW_NAME_NEWSFORM);
		return model;
	}

	/**
	 * Edits the news.
	 *
	 * @param newsId
	 *            the news id
	 * @param model
	 *            the model and view object
	 * @return the model and view object
	 * @throws ServiceException
	 *             the service exception if operation failed
	 */
	@RequestMapping(value = { ADMIN + "/editnews/{newsId}" })
	public ModelAndView editNews(@PathVariable long newsId, ModelAndView model) throws ServiceException {
		NewsDTO newsDTO = newsManagementService.findNews(newsId);
		News news = newsDTO.getNews();
		news.setModificationDate(new Date());
		prepareFormView(model, newsDTO.getAuthor().getAuthorId());
		model.addObject(MODEL_ATTRIBUTE_NEWS_DTO, newsDTO);
		model.setViewName(VIEW_NAME_NEWSFORM);
		return model;
	}

	/**
	 * Process news.
	 *
	 * @param newsDTO
	 *            the news DTO object
	 * @param result
	 *            the result
	 * @param model
	 *            the model and view object
	 * @return the model and view object
	 * @throws ServiceException
	 *             the service exception if operation failed
	 */
	@RequestMapping(value = { ADMIN + "/processnews" })
	public ModelAndView processNews(@Valid @ModelAttribute NewsDTO newsDTO, BindingResult result, ModelAndView model)
			throws ServiceException {
		Long authorId = newsDTO.getAuthor().getAuthorId();
		if (result.hasErrors() || authorId == 0) {
			model = errorAction(model, authorId, newsDTO);
		} else {
			model = validAction(model, newsDTO);
		}
		return model;
	}

	/**
	 * Inits the binder.
	 *
	 * @param binder
	 *            the web data binder
	 */
	@InitBinder
	public void initBinder(WebDataBinder binder) {
		Locale locale = LocaleContextHolder.getLocale();
		SimpleDateFormat sdf = locale.equals(Locale.ENGLISH) ? new SimpleDateFormat(MM_DD_YYYY)
				: new SimpleDateFormat(DD_MM_YYYY);
		sdf.setLenient(true);
		binder.registerCustomEditor(Date.class, new CustomDateEditor(sdf, true));
		binder.registerCustomEditor(List.class, NEWS_DTO_FIELD_TAGS, new CustomCollectionEditor(List.class) {
			@Override
			protected Object convertElement(Object element) {
				Tag tag = null;
				if (element instanceof Tag) {
					tag = (Tag) element;
				}
				if (element instanceof String) {
					tag = new Tag();
					tag.setTagId(Long.parseLong((String) element));
				}
				return tag;
			}
		});
	}

	/**
	 * Error action.
	 *
	 * @param model
	 *            the model and view object
	 * @param authorId
	 *            the author id
	 * @param newsDTO
	 *            the news DTO object
	 * @return the model and view object
	 * @throws ServiceException
	 *             the service exception if operation failed
	 */
	private ModelAndView errorAction(ModelAndView model, Long authorId, NewsDTO newsDTO) throws ServiceException {
		if (authorId == 0) {
			model.addObject(MODEL_ATTRIBUTE_SELECT_AUTHOR_MSG, MESSAGE_AUTHOR_HAVEN_T_BEEN_CHOSEN);
		}
		prepareFormView(model, authorId);
		model.addObject(MODEL_ATTRIBUTE_NEWS_DTO, newsDTO);
		model.setViewName(VIEW_NAME_NEWSFORM);
		return model;
	}

	/**
	 * Valid action.
	 *
	 * @param model
	 *            the model and view object
	 * @param newsDTO
	 *            the news DTO object
	 * @return the model and view object
	 * @throws ServiceException
	 *             the service exception if operation failed
	 */
	private ModelAndView validAction(ModelAndView model, NewsDTO newsDTO) throws ServiceException {
		News news = newsDTO.getNews();
		Long newsId = news.getNewsId();
		if (newsId == null) {
			news.setCreationDate(news.getModificationDate());
			newsDTO.setNews(news);
			newsId = newsManagementService.addNews(newsDTO);
		} else {
			newsDTO.setNews(news);
			newsManagementService.editNews(newsDTO);
		}
		newsDTO = newsManagementService.findNews(newsId);
		model.addObject(MODEL_ATTRIBUTE_NEWS_DTO, newsDTO);
		model.setViewName(VIEW_NAME_SHOWNEWS);
		return model;
	}

	/**
	 * Prepare form view.
	 *
	 * @param model
	 *            the model and view object
	 * @param authorId
	 *            the author id
	 * @return the model and view object
	 * @throws ServiceException
	 *             the service exception if operation failed
	 */
	private ModelAndView prepareFormView(ModelAndView model, Long authorId) throws ServiceException {
		List<Author> authorList = newsManagementService.findAllAuthor();
		authorList = removeExpired(authorList, authorId);
		List<Tag> tagList = newsManagementService.findAllTag();
		model.addObject(MODEL_ATTRIBUTE_AUTHOR_LIST, authorList);
		model.addObject(MODEL_ATTRIBUTE_TAG_LIST, tagList);
		return model;
	}

	/**
	 * Removes the expired authors.
	 *
	 * @param authorList
	 *            the author object list
	 * @param authorId
	 *            the author id
	 * @return the author object list
	 */
	private List<Author> removeExpired(List<Author> authorList, Long authorId) {
		Iterator<Author> authorListIterator = authorList.iterator();
		while (authorListIterator.hasNext()) {
			Author author = authorListIterator.next();
			if (author.getExpired() != null & author.getAuthorId() != authorId) {
				authorListIterator.remove();
			}
		}
		return authorList;
	}
}
