package by.epam.newsadmin.controller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import by.epam.newscommon.exception.ServiceException;
import by.epam.newscommon.service.NewsManagementService;
import by.epam.newsdao.entity.Author;

/**
 * The Class AuthorFormController.
 */
@Controller
public class AuthorFormController {

	/** The Constant ADMIN. */
	private static final String ADMIN = "/admin/";

	/** The Constant MODEL_ATTRIBUTE_EDIT_AUTHOR_ID. */
	private static final String MODEL_ATTRIBUTE_EDIT_AUTHOR_ID = "editAuthorId";

	/** The Constant MODEL_ATTRIBUTE_AUTHOR. */
	private static final String MODEL_ATTRIBUTE_AUTHOR = "author";

	/** The Constant MODEL_ATTRIBUTE_AUTHOR_LIST. */
	private static final String MODEL_ATTRIBUTE_AUTHOR_LIST = "authorList";

	/** The Constant VIEW_NAME_AUTHORFORM. */
	private static final String VIEW_NAME_AUTHORFORM = "authorform";

	/** The news management service. */
	@Autowired
	private NewsManagementService newsManagementService;

	/**
	 * Adds the author.
	 *
	 * @param authorId
	 *            the author id
	 * @param authorName
	 *            the author name
	 * @param model
	 *            the model and view object
	 * @param session
	 *            the session
	 * @return the model and view object
	 * @throws ServiceException
	 *             the service exception if operation failed
	 */
	@RequestMapping(value = { ADMIN + "addauthor" })
	public ModelAndView addAuthor(@RequestParam(required = false) Long authorId,
			@RequestParam(required = false) String authorName, ModelAndView model, HttpSession session)
					throws ServiceException {
		if (authorId != null) {
			newsManagementService.makeAuthorExpired(new Author(authorId, authorName, new Date()));
		}
		model.addObject(MODEL_ATTRIBUTE_AUTHOR, new Author());
		return prepareView(model);
	}

	/**
	 * Edits the author.
	 *
	 * @param authorId
	 *            the author id
	 * @param authorName
	 *            the author name
	 * @param model
	 *            the model and view object
	 * @return the model and view object
	 * @throws ServiceException
	 *             the service exception if operation failed
	 */
	@RequestMapping(value = { ADMIN + "editauthor/{authorId}" })
	public ModelAndView editAuthor(@PathVariable long authorId, @RequestParam(required = false) String authorName,
			ModelAndView model) throws ServiceException {
		model.addObject(MODEL_ATTRIBUTE_EDIT_AUTHOR_ID, authorId);
		model.addObject(MODEL_ATTRIBUTE_AUTHOR, new Author(authorId, authorName, null));
		return prepareView(model);
	}

	/**
	 * Process author.
	 *
	 * @param author
	 *            the author object
	 * @param result
	 *            the result
	 * @param model
	 *            the model abd view object
	 * @param session
	 *            the session
	 * @return the model and view object
	 * @throws ServiceException
	 *             the service exception if operation failed
	 */
	@RequestMapping(value = { ADMIN + "processauthor" })
	public ModelAndView processAuthor(@Valid @ModelAttribute Author author, BindingResult result, ModelAndView model,
			HttpSession session) throws ServiceException {
		Long authorId = author.getAuthorId();
		if (result.hasErrors()) {
			model = errorAction(model, authorId);
		} else {
			model = validAction(model, author);
		}
		return prepareView(model);
	}

	/**
	 * Error action.
	 *
	 * @param model
	 *            the model and view object
	 * @param authorId
	 *            the author id
	 * @return the model and view object
	 * @throws ServiceException
	 *             the service exception if operation failed
	 */
	private ModelAndView errorAction(ModelAndView model, Long authorId) throws ServiceException {
		if (authorId != null) {
			model.addObject(MODEL_ATTRIBUTE_EDIT_AUTHOR_ID, authorId);
		}
		return model;
	}

	/**
	 * Valid action.
	 *
	 * @param model
	 *            the model and view object
	 * @param author
	 *            the author object
	 * @return the model and view object
	 * @throws ServiceException
	 *             the service exception if operation failed
	 */
	private ModelAndView validAction(ModelAndView model, Author author) throws ServiceException {
		if (author.getAuthorId() != null) {
			newsManagementService.updateAuthor(author);
		} else {
			newsManagementService.saveAuthor(author);
		}
		model.addObject(MODEL_ATTRIBUTE_AUTHOR, new Author());
		return model;
	}

	/**
	 * Prepare view.
	 *
	 * @param model
	 *            the model and view object
	 * @return the model and view object
	 * @throws ServiceException
	 *             the service exception if operation failed
	 */
	private ModelAndView prepareView(ModelAndView model) throws ServiceException {
		List<Author> authorList = newsManagementService.findAllAuthor();
		model.addObject(MODEL_ATTRIBUTE_AUTHOR_LIST, authorList);
		model.setViewName(VIEW_NAME_AUTHORFORM);
		return model;
	}
}
