package by.epam.newsadmin.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import by.epam.newsadmin.service.NewsPaginationProcessor;
import by.epam.newscommon.exception.ServiceException;
import by.epam.newscommon.service.NewsManagementService;
import by.epam.newsdao.entity.*;

/**
 * The Class ListNewsController.
 */
@Controller
public class ListNewsController {
	/** The Constant ADMIN. */
	private static final String ADMIN = "/admin/";

	/** The Constant VIEW_NAME_LISTNEWS. */
	private static final String VIEW_NAME_LISTNEWS = "listnews";

	/** The Constant VIEW_NAME_REDIRECT_ADMIN_LISTNEWS. */
	private static final String VIEW_NAME_REDIRECT_ADMIN_LISTNEWS = "redirect:/admin/listnews";

	/** The Constant MODEL_ATTRIBUTE_TAG_LIST. */
	private static final String MODEL_ATTRIBUTE_TAG_LIST = "tagList";

	/** The Constant MODEL_ATTRIBUTE_AUTHOR_LIST. */
	private static final String MODEL_ATTRIBUTE_AUTHOR_LIST = "authorList";

	/** The Constant SESSION_ATTRIBUTE_CURRENT_PAGE. */
	private static final String SESSION_ATTRIBUTE_CURRENT_PAGE = "currentPage";

	/** The Constant SESSION_ATTRIBUTE_NO_OF_PAGES. */
	private static final String SESSION_ATTRIBUTE_NO_OF_PAGES = "noOfPages";

	/** The Constant SESSION_ATTRIBUTE_OFFSET. */
	private static final String SESSION_ATTRIBUTE_OFFSET = "offSet";

	/** The Constant SESSION_ATTRIBUTE_NEWS_DTO_LIST. */
	private static final String SESSION_ATTRIBUTE_NEWS_DTO_LIST = "newsDTOList";
	/** The Constant NEWS_AT_PAGE. */
	public static final int NEWS_AT_PAGE = 3;

	/** The pagination processor. */
	@Autowired
	private NewsPaginationProcessor processor;

	/** The news management service. */
	@Autowired
	private NewsManagementService newsManagementService;

	/**
	 * List news.
	 *
	 * @param pageNumber
	 *            the page number
	 * @param model
	 *            the model and view object
	 * @param request
	 *            the request
	 * @param session
	 *            the session
	 * @return the model and view object
	 * @throws ServiceException
	 *             the service exception if operation failed
	 */
	@RequestMapping(value = { ADMIN + "listnews" }, method = RequestMethod.GET)
	public ModelAndView listNews(
			@RequestParam(defaultValue = "1") int pageNumber,
			ModelAndView model, HttpServletRequest request, HttpSession session)
			throws ServiceException {
		List<Author> authorList = newsManagementService.findAllAuthor();
		List<Tag> tagList = newsManagementService.findAllTag();
		int offSet = (pageNumber - 1) * NEWS_AT_PAGE;
		List<NewsDTO> newsDTOList = processor.processNewsForPage(request,
				offSet, NEWS_AT_PAGE);
		long newsCount = processor.getNewsCount();
		long noOfPages = newsCount % NEWS_AT_PAGE == 0 ? newsCount
				/ NEWS_AT_PAGE : (newsCount / NEWS_AT_PAGE) + 1;
		session.setAttribute(SESSION_ATTRIBUTE_NEWS_DTO_LIST, newsDTOList);
		session.setAttribute(SESSION_ATTRIBUTE_OFFSET, offSet);
		session.setAttribute(SESSION_ATTRIBUTE_NO_OF_PAGES, noOfPages);
		session.setAttribute(SESSION_ATTRIBUTE_CURRENT_PAGE, pageNumber);
		model.addObject(MODEL_ATTRIBUTE_AUTHOR_LIST, authorList);
		model.addObject(MODEL_ATTRIBUTE_TAG_LIST, tagList);
		model.setViewName(VIEW_NAME_LISTNEWS);
		return model;
	}

	/**
	 * Delete news.
	 *
	 * @param deleteNewsId
	 *            the news id for deletion
	 * @param model
	 *            the model and view object
	 * @return the model and view object
	 * @throws ServiceException
	 *             the service exception if operation failed
	 */
	@RequestMapping(value = { ADMIN + "deletenews" }, method = RequestMethod.POST)
	public ModelAndView deleteNews(
			@RequestParam(required = false) Long[] deleteNewsId,
			ModelAndView model) throws ServiceException {
		if (deleteNewsId != null) {
			newsManagementService.deleteNews(deleteNewsId);
		}
		model.setViewName(VIEW_NAME_REDIRECT_ADMIN_LISTNEWS);
		return model;
	}
}
