package by.epam.newsadmin.controller;

import java.util.List;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import by.epam.newscommon.exception.ServiceException;
import by.epam.newscommon.service.NewsManagementService;
import by.epam.newsdao.entity.Tag;

/**
 * The Class TagFormController.
 */
@Controller
public class TagFormController {
	/** The Constant ADMIN. */
	private static final String ADMIN = "/admin/";
	
	/** The Constant MODEL_ATTRIBUTE_EDIT_TAG_ID. */
	private static final String MODEL_ATTRIBUTE_EDIT_TAG_ID = "editTagId";

	/** The Constant VIEW_NAME_TAGFORM. */
	private static final String VIEW_NAME_TAGFORM = "tagform";

	/** The Constant MODEL_ATTRIBUTE_TAG. */
	private static final String MODEL_ATTRIBUTE_TAG = "tag";

	/** The Constant MODEL_ATTRIBUTE_TAGS. */
	private static final String MODEL_ATTRIBUTE_TAGS = "tags";

	/** The news management service. */
	@Autowired
	private NewsManagementService newsManagementService;

	/**
	 * Adds the tag.
	 *
	 * @param tagId
	 *            the tag id
	 * @param model
	 *            the model and view object
	 * @param session
	 *            the session
	 * @return the model and view object
	 * @throws ServiceException
	 *             the service exception if operation failed
	 */
	@RequestMapping(value = { ADMIN + "addtag" })
	public ModelAndView addTag(@RequestParam(required = false) Long tagId, ModelAndView model, HttpSession session)
			throws ServiceException {
		if (tagId != null) {
			newsManagementService.deleteTag(tagId);
		}
		List<Tag> tagList = newsManagementService.findAllTag();
		model.addObject(MODEL_ATTRIBUTE_TAGS, tagList);
		model.addObject(MODEL_ATTRIBUTE_TAG, new Tag());
		model.setViewName(VIEW_NAME_TAGFORM);
		return model;
	}

	/**
	 * Edits the tag.
	 *
	 * @param tagId
	 *            the tag id
	 * @param tagName
	 *            the tag name
	 * @param model
	 *            the model and view object
	 * @return the model and view object
	 * @throws ServiceException
	 *             the service exception if operation failed
	 */
	@RequestMapping(value = { ADMIN + "edittag/{tagId}" })
	public ModelAndView editTag(@PathVariable long tagId, @RequestParam(required = false) String tagName,
			ModelAndView model) throws ServiceException {
		model.addObject(MODEL_ATTRIBUTE_EDIT_TAG_ID, tagId);
		model.addObject(MODEL_ATTRIBUTE_TAG, new Tag(tagId, tagName));
		return prepareView(model);
	}

	/**
	 * Process tag.
	 *
	 * @param tag
	 *            the tag
	 * @param result
	 *            the result
	 * @param model
	 *            the model and view object
	 * @param session
	 *            the session
	 * @return the model and view object
	 * @throws ServiceException
	 *             the service exception if operation failed
	 */
	@RequestMapping(value = { ADMIN + "processtag" })
	public ModelAndView processTag(@Valid @ModelAttribute Tag tag, BindingResult result, ModelAndView model,
			HttpSession session) throws ServiceException {
		Long tagId = tag.getTagId();
		if (result.hasErrors()) {
			model = errorAction(model, tagId);
		} else {
			model = validAction(model, tag);
		}
		return prepareView(model);
	}

	/**
	 * Error action.
	 *
	 * @param model
	 *            the model and view object
	 * @param tagId
	 *            the tag id
	 * @return the model and view object
	 * @throws ServiceException
	 *             the service exception if operation failed
	 */
	private ModelAndView errorAction(ModelAndView model, Long tagId) throws ServiceException {
		if (tagId != null) {
			model.addObject(MODEL_ATTRIBUTE_EDIT_TAG_ID, tagId);
		}
		return model;
	}

	/**
	 * Valid action.
	 *
	 * @param model
	 *            the model and view object
	 * @param tag
	 *            the tag object
	 * @return the model and view object
	 * @throws ServiceException
	 *             the service exception if operation failed
	 */
	private ModelAndView validAction(ModelAndView model, Tag tag) throws ServiceException {
		if (tag.getTagId() != null) {
			newsManagementService.updateTag(tag);
		} else {
			newsManagementService.saveTag(tag);
		}
		model.addObject(MODEL_ATTRIBUTE_TAG, new Tag());
		return model;
	}

	/**
	 * Prepare view.
	 *
	 * @param model
	 *            the model and view object
	 * @return the model and view object
	 * @throws ServiceException
	 *             the service exception if operation failed
	 */
	private ModelAndView prepareView(ModelAndView model) throws ServiceException {
		List<Tag> tagList = newsManagementService.findAllTag();
		model.addObject(MODEL_ATTRIBUTE_TAGS, tagList);
		model.setViewName(VIEW_NAME_TAGFORM);
		return model;
	}
}
