package by.epam.newsadmin.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import by.epam.newscommon.exception.ServiceException;
import by.epam.newscommon.service.NewsManagementService;
import by.epam.newsdao.entity.NewsDTO;
import by.epam.newsdao.entity.SearchCriteria;

/**
 * The Class NewsPaginationProcessor.
 */
@Component
public class NewsPaginationProcessor {
	private static final String SESSION_ATTRIBUTE_NEWS_DTO_LIST = "newsDTOList";
	private static final String VIEW_DIRECTION_NEXT = "next";
	private static final String VIEW_DIRECTION_PREV = "prev";
	private static final String SESSION_ATTRUBUTE_OFF_SET = "offSet";
	private static final String REQUEST_PARAMETER_TAGS = "tags";
	private static final String REQUEST_PARAMETER_AUTHOR_ID = "authorId";
	private static final String REQUEST_PARAMETER_CLEAR = "clear";
	private static final String REQUEST_PARAMETER_SEARCH = "search";
	private static final String SESSION_ATTRIBUTE_SEARCH_CRITERIA = "searchCriteria";

	/** The news count. */
	private long newsCount;

	/**
	 * Gets the news count.
	 *
	 * @return the news count
	 */
	public long getNewsCount() {
		return newsCount;
	}

	/** The news management service. */
	@Autowired
	private NewsManagementService newsManagementService;
	
	/** The entity initializer. */
	@Autowired
	private EntityInitializer initializer;

	/**
	 * Process news for page.
	 *
	 * @param request the request
	 * @param offset the offset
	 * @param newsAtPage the quantity of news at page
	 * @return the list of newsDTO object 
	 * @throws ServiceException the service exception if operation failed
	 */
	public List<NewsDTO> processNewsForPage(HttpServletRequest request,
			int offset, int newsAtPage) throws ServiceException {
		HttpSession session = request.getSession();
		List<NewsDTO> newsDTOList = null;
		SearchCriteria searchCriteria = (SearchCriteria) session
				.getAttribute(SESSION_ATTRIBUTE_SEARCH_CRITERIA);
		String search = request.getParameter(REQUEST_PARAMETER_SEARCH);
		String clear = request.getParameter(REQUEST_PARAMETER_CLEAR);
		String authId = request.getParameter(REQUEST_PARAMETER_AUTHOR_ID);
		Long authorId = authId != null ? Long.parseLong(authId) : null;
		String[] tags = request.getParameterValues(REQUEST_PARAMETER_TAGS);
		searchCriteria = clear != null ? null : searchCriteria;
		searchCriteria = search != null ? initializer.initSearchCriteria(
				authorId, tags) : searchCriteria;
		if (searchCriteria != null) {
			newsCount = newsManagementService.countNews(searchCriteria);
			newsDTOList = newsManagementService.findNewsBySearchCriteria(
					searchCriteria, offset, newsAtPage);
		} else {
			newsCount = newsManagementService.countNews();
			newsDTOList = newsManagementService.findAllNews(offset, newsAtPage);
		}
		session.setAttribute(SESSION_ATTRIBUTE_SEARCH_CRITERIA, searchCriteria);
		return newsDTOList;
	}

	/**
	 * Calculate offset.
	 *
	 * @param newsId the news id
	 * @param direction the news view direction 
	 * @param session the session
	 * @return the integer
	 */
	public Integer calculateOffSet(Long newsId, String direction,
			HttpSession session) {
		Integer offSet = (Integer) session.getAttribute(SESSION_ATTRUBUTE_OFF_SET);
		if (direction.equals(VIEW_DIRECTION_PREV) | direction.equals(VIEW_DIRECTION_NEXT)) {
			offSet = direction.equals(VIEW_DIRECTION_PREV) ? --offSet : offSet;
			offSet = direction.equals(VIEW_DIRECTION_NEXT) ? ++offSet : offSet;
		} else {
			@SuppressWarnings("unchecked")
			List<NewsDTO> newsDTOList = (List<NewsDTO>) session
					.getAttribute(SESSION_ATTRIBUTE_NEWS_DTO_LIST);
			if (newsDTOList != null) {
				for (int i = 0; i < newsDTOList.size(); i++) {
					NewsDTO tempNewsDTO = newsDTOList.get(i);
					if (tempNewsDTO.getNews().getNewsId() == newsId) {
						offSet += i;
					}
				}
			}
		}
		return offSet;
	}
}
