package by.epam.newsadmin.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import by.epam.newsdao.entity.*;

// TODO: Auto-generated Javadoc
/**
 * The Class EntityInitializer.
 */
@Component
public class EntityInitializer {

	/**
	 * Inits the search criteria.
	 *
	 * @param authorId the author id
	 * @param tags the tags
	 * @return the search criteria
	 */
	public SearchCriteria initSearchCriteria(Long authorId, String[] tags) {
		return new SearchCriteria(initAuthor(authorId), initTags(tags));
	}

	/**
	 * Inits the news dto.
	 *
	 * @param authorId the author id
	 * @param tags the tags
	 * @return the news dto
	 */
	public NewsDTO initNewsDTO(Long authorId, String[] tags) {
		return new NewsDTO(null, initAuthor(authorId), initTags(tags), null);
	}

	/**
	 * Inits the author.
	 *
	 * @param authorId the author id
	 * @return the author
	 */
	private Author initAuthor(Long authorId) {
		return authorId != 0 ? new Author(authorId, null, null) : null;
	}

	/**
	 * Inits the tags.
	 *
	 * @param tags the tags
	 * @return the list
	 */
	private List<Tag> initTags(String[] tags) {
		List<Tag> tagList = null;
		if (tags != null) {
			tagList = new ArrayList<>();
			for (int i = 0; i < tags.length; i++) {
				Long tagId = tags[i] != null ? Long.parseLong(tags[i]) : null;
				tagList.add(new Tag(tagId, null));
			}
		}
		return tagList;
	}
}
