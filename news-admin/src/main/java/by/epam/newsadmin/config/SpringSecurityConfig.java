package by.epam.newsadmin.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@EnableWebSecurity
public class SpringSecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	DataSource dataSource;

	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		Md5PasswordEncoder pe = new Md5PasswordEncoder();
		String usersQuery = "select	users.login, users.password, 1 as enabled from users where users.login=?";
		String authoritiesQuery = "select users.login, users.role from users where users.login=?";
		auth.jdbcAuthentication().passwordEncoder(pe).dataSource(dataSource).usersByUsernameQuery(usersQuery)
				.authoritiesByUsernameQuery(authoritiesQuery);
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests().antMatchers("/admin/**").access("hasRole('ROLE_ADMIN')").and().formLogin()
				.loginProcessingUrl("/j_spring_security_check").loginPage("/login").defaultSuccessUrl("/admin/listnews")
				.failureUrl("/login?error").usernameParameter("login").passwordParameter("password").and().logout()
				.logoutUrl("/j_spring_security_logout").logoutSuccessUrl("/login?logout").and().csrf();
	}
}
