package by.epam.newsdao.dao.impl;

import static org.junit.Assert.*;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.*;

import by.epam.newsdao.dao.NewsDAO;
import by.epam.newsdao.entity.*;
import by.epam.newsdao.exception.DAOException;

/**
 * The Class NewsDAOTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({ "classpath:spring-testconfig.xml" })
@DbUnitConfiguration(databaseConnection = "dbUnitDatabaseConnection")
@DatabaseSetup(value = "/dbunitNewsData.xml")
@DatabaseTearDown(value = "/dbunitNewsData.xml", type = DatabaseOperation.DELETE_ALL)
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class,
		DbUnitTestExecutionListener.class })
public class NewsDAOTest {

	/** The news DAO. */
	@Autowired
	private NewsDAO newsDAO;

	/**
	 * Test find.
	 *
	 * @throws Exception
	 *             the DAO exception if database operation failed
	 */
	@Test
	public void testFind() throws DAOException {
		News expectedNews = new News(1L, "title1", "short text1", "full text1",
				Timestamp.valueOf("2015-08-10 12:46:29.77"),
				Date.valueOf("2015-08-10"));
		News actualNews = newsDAO.find(1L);
		assertEquals(actualNews, expectedNews);
	}

	/**
	 * Test find all.
	 *
	 * @throws Exception
	 *             the DAO exception if database operation failed
	 */
	@Test
	public void testFindAll() throws DAOException {
		List<News> expectedNewsList = newsDAO.findAll();
		assertEquals(expectedNewsList.size(), 3);
	}

	/**
	 * Test find all with pagination.
	 *
	 * @throws Exception
	 *             the DAO exception if database operation failed
	 */
	@Test
	public void testFindAllWithPagination() throws DAOException {
		List<News> expectedNewsList = newsDAO.findAll(0, 2);
		assertEquals(expectedNewsList.size(), 2);
	}

	/**
	 * Test save.
	 *
	 * @throws Exception
	 *             the DAO exception if database operation failed
	 */
	@Test
	public void testSave() throws DAOException {
		News expectedNews = new News(3L, "title", "short text", "full text",
				Timestamp.valueOf("2015-08-26 09:46:29.77"),
				Date.valueOf("2015-08-26"));
		long newsId = newsDAO.save(expectedNews);
		expectedNews.setNewsId(newsId);
		News actualNews = newsDAO.find(newsId);
		assertEquals(actualNews, expectedNews);
	}

	/**
	 * Test update.
	 *
	 * @throws Exception
	 *             the DAO exception if database operation failed
	 */
	@Test
	public void testUpdate() throws DAOException {
		News expectedNews = new News(1L, "new title", "new short text",
				"new full text", Timestamp.valueOf("2015-08-10 12:46:29.77"),
				Date.valueOf("2015-08-26"));
		newsDAO.update(expectedNews);
		News actualNews = newsDAO.find(1L);
		assertEquals(actualNews, expectedNews);
	}

	/**
	 * Test delete.
	 *
	 * @throws Exception
	 *             the DAO exception if database operation failed
	 */
	@Test
	public void testDelete() throws DAOException {
		News actualNews = newsDAO.find(3L);
		assertNotNull(actualNews);
		newsDAO.delete(3L);
		actualNews = newsDAO.find(3L);
		assertNull(actualNews);
	}

	/**
	 * Test delete.
	 *
	 * @throws Exception
	 *             the DAO exception if database operation failed
	 */
	@Test
	public void testDeleteArrayOfId() throws DAOException {
		News actualNews = newsDAO.find(3L);
		assertNotNull(actualNews);
		Long[] newsId = new Long[] { 3L };
		newsDAO.delete(newsId);
		actualNews = newsDAO.find(3L);
		assertNull(actualNews);
	}

	/**
	 * Test find by search criteria.
	 *
	 * @throws Exception
	 *             the DAO exception if database operation failed
	 */
	@Test
	public void testFindBySearchCriteria() throws DAOException {
		SearchCriteria sc = new SearchCriteria();
		sc.setAuthor(new Author(1L, "john doe", null));
		List<Tag> tagList = new ArrayList<>();
		tagList.add(new Tag(2L, "SECOND TAG"));
		sc.setTagList(tagList);
		List<News> actualNewsList = newsDAO.findBySearchCriteria(sc, 0, 1);
		assertEquals(actualNewsList.size(), 1);
	}

	/**
	 * Test news count.
	 *
	 * @throws Exception
	 *             the DAO exception if database operation failed
	 */
	@Test
	public void testNewsCount() throws DAOException {
		long actualNewsCount = newsDAO.newsCount();
		assertEquals(actualNewsCount, 3);
	}

	/**
	 * Test news count with search criteria.
	 *
	 * @throws Exception
	 *             the DAO exception if database operation failed
	 */
	@Test
	public void testNewsCountWithSearchCriteria() throws DAOException {
		SearchCriteria sc = new SearchCriteria();
		sc.setAuthor(new Author(1L, "john doe", null));
		List<Tag> tagList = new ArrayList<>();
		tagList.add(new Tag(2L, "SECOND TAG"));
		sc.setTagList(tagList);
		long actualNewsCount = newsDAO.newsCount(sc);
		assertEquals(actualNewsCount, 1);
	}

	/**
	 * Test save news author.
	 *
	 * @throws Exception
	 *             the DAO exception if database operation failed
	 */
	@Test
	public void testSaveNewsAuthor() throws DAOException {
		newsDAO.saveNewsAuthor(3l, 2L);
		long actualAuthorId = newsDAO.findAuthorIdByNewsId(3L);
		assertEquals(actualAuthorId, 2L);
	}

	/**
	 * Test update news author.
	 *
	 * @throws Exception
	 *             the DAO exception if database operation failed
	 */
	@Test
	public void testUpdateNewsAuthor() throws DAOException {
		newsDAO.updateNewsAuthor(2l, 2L);
		long actualAuthorId = newsDAO.findAuthorIdByNewsId(2L);
		assertEquals(actualAuthorId, 2L);
	}

	/**
	 * Test delete news author.
	 *
	 * @throws Exception
	 *             the DAO exception if database operation failed
	 */
	@Test
	public void testDeleteNewsAuthor() throws DAOException {
		Long actualAuthorId = newsDAO.findAuthorIdByNewsId(1L);
		assertEquals(actualAuthorId.longValue(), 1L);
		Long[] newsId = new Long[] { 1L };
		newsDAO.deleteNewsAuthor(newsId);
		actualAuthorId = newsDAO.findAuthorIdByNewsId(1L);
		assertNull(actualAuthorId);
	}

	/**
	 * Test find tag id by news id.
	 *
	 * @throws Exception
	 *             the DAO exception if database operation failed
	 */
	@Test
	public void testFindTagIdByNewsId() throws DAOException {
		List<Long> tagIdList = newsDAO.findTagIdByNewsId(1l);
		assertEquals(tagIdList.size(), 2);
	}

	/**
	 * Test save news tag.
	 *
	 * @throws Exception
	 *             the DAO exception if database operation failed
	 */
	@Test
	public void testSaveNewsTag() throws DAOException {
		newsDAO.saveNewsTag(3L, 3L);
		List<Long> tagIdList = newsDAO.findTagIdByNewsId(3l);
		long actualTagId = tagIdList.get(0);
		assertEquals(actualTagId, 3);
	}

	/**
	 * Test update news tag.
	 *
	 * @throws Exception
	 *             the DAO exception if database operation failed
	 */
	@Test
	public void testUpdateNewsTag() throws DAOException {
		List<Long> tagIdList = newsDAO.findTagIdByNewsId(2l);
		long actualTagId = tagIdList.get(0);
		assertEquals(actualTagId, 2);
		newsDAO.updateNewsTag(2L, 3L);
		tagIdList = newsDAO.findTagIdByNewsId(2l);
		actualTagId = tagIdList.get(0);
		assertEquals(actualTagId, 3);
	}

	/**
	 * Test delete news tag.
	 *
	 * @throws Exception
	 *             the DAO exception if database operation failed
	 */
	@Test
	public void testDeleteNewsTag() throws DAOException {
		List<Long> actualTagIdList = newsDAO.findTagIdByNewsId(1l);
		assertEquals(actualTagIdList.size(), 2);
		Long[] newsId = new Long[] { 1L };
		newsDAO.deleteNewsTagByNewsId(newsId);
		actualTagIdList = newsDAO.findTagIdByNewsId(1l);
		assertEquals(actualTagIdList.size(), 0);
	}

	/**
	 * Test delete news tag by tag id.
	 *
	 * @throws Exception
	 *             the DAO exception if database operation failed
	 */
	@Test
	public void testDeleteNewsTagByTagId() throws DAOException {
		List<Long> actualTagIdList = newsDAO.findTagIdByNewsId(1l);
		assertEquals(actualTagIdList.size(), 2);
		newsDAO.deleteNewsTagByTagId(actualTagIdList.get(0));
		newsDAO.deleteNewsTagByTagId(actualTagIdList.get(1));
		actualTagIdList = newsDAO.findTagIdByNewsId(1l);
		assertEquals(actualTagIdList.size(), 0);
	}

	/**
	 * Test find author id by news id.
	 *
	 * @throws Exception
	 *             the DAO exception if database operation failed
	 */
	@Test
	public void testFindAuthorIdByNewsId() throws DAOException {
		long actualAuthorId = newsDAO.findAuthorIdByNewsId(2L);
		assertEquals(actualAuthorId, 1);
	}

	/**
	 * Test update exception.
	 *
	 * @throws DAO
	 *             exception database operation failed
	 */
	@Test(expected = DAOException.class)
	public void testUpdateException() throws DAOException {
		News expectedNews = new News(3L,
				"title fffffffffffffffffffffffffffffffffffffff", "short text",
				"full text", Timestamp.valueOf("2015-08-26 09:46:29.77"),
				Date.valueOf("2015-08-26"));
		newsDAO.update(expectedNews);
	}

	/**
	 * Test save exception.
	 *
	 * @throws DAO
	 *             exception database operation failed
	 */
	@Test(expected = DAOException.class)
	public void testSaveException() throws DAOException {
		News expectedNews = new News(3L,
				"title ffffffffffffffffffffffffffffffffffffffffff",
				"short text", "full text",
				Timestamp.valueOf("2015-08-26 09:46:29.77"),
				Date.valueOf("2015-08-26"));
		newsDAO.save(expectedNews);
	}

	/**
	 * Test delete exception.
	 *
	 * @throws DAO
	 *             exception database operation failed
	 */
	@Test(expected = DAOException.class)
	public void testDeleteException() throws DAOException {
		newsDAO.delete(1L);
	}
}