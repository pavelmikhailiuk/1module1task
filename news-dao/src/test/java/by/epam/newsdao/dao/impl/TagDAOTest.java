package by.epam.newsdao.dao.impl;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import by.epam.newsdao.dao.TagDAO;
import by.epam.newsdao.entity.Tag;
import by.epam.newsdao.exception.DAOException;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.*;

/**
 * The Class TagDAOTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({ "classpath:spring-testconfig.xml" })
@DbUnitConfiguration(databaseConnection = "dbUnitDatabaseConnection")
@DatabaseSetup(value = "/dbunitTagData.xml")
@DatabaseTearDown(value = "/dbunitTagData.xml", type = DatabaseOperation.DELETE_ALL)
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class,
		DbUnitTestExecutionListener.class })
public class TagDAOTest {
	
	/** The tag DAO. */
	@Autowired
	private TagDAO tagDAO;

		/**
		 * Test find.
		 *
		 * @throws Exception the DAO exception if database operation failed 
		 */
		@Test
	public void testFind() throws DAOException {
		Tag expectedTag = new Tag(1L, "FIRST TAG");
		Tag actualTag = tagDAO.find(1L);
		assertEquals(actualTag, expectedTag);
	}

	/**
	 * Test get all.
	 *
	 * @throws Exception the DAO exception if database operation failed 
	 */
	@Test
	public void testGetAll() throws DAOException {
		List<Tag> tagList = tagDAO.findAll();
		assertEquals(tagList.size(),
				2);
	}

	/**
	 * Test save.
	 *
	 * @throws Exception the DAO exception if database operation failed 
	 */
	@Test
	public void testSave() throws DAOException {
		Tag expectedTag = new Tag(3L, "third tag");
		long tagId = tagDAO.save(expectedTag);
		expectedTag.setTagId(tagId);
		Tag actualTag = tagDAO.find(tagId);
		assertEquals(actualTag, expectedTag);
	}

	/**
	 * Test update.
	 *
	 * @throws Exception the DAO exception if database operation failed 
	 */
	@Test
	public void testUpdate() throws DAOException {
		Tag expectedTag = new Tag(1L, "third tag");
		tagDAO.update(expectedTag);
		Tag actualTag = tagDAO.find(1L);
		assertEquals(actualTag, expectedTag);
	}

	/**
	 * Test delete.
	 *
	 * @throws Exception the DAO exception if database operation failed 
	 */
	@Test
	public void testDelete() throws DAOException {
		Tag actualTag = tagDAO.find(1L);
		assertNotNull(actualTag);
		tagDAO.delete(1L);
		actualTag = tagDAO.find(1L);
		assertNull(actualTag);
	}
	/**
	 * Test update exception.
	 *
	 * @throws DAO
	 *             exception database operation failed
	 */
	@Test(expected = DAOException.class)
	public void testUpdateException() throws DAOException {
		Tag expectedTag = new Tag(1L, "third tag ggggggggggggggggggggggggggggggggggggggggggggg");
		tagDAO.update(expectedTag);
	}

	/**
	 * Test save exception.
	 *
	 * @throws DAO
	 *             exception database operation failed
	 */
	@Test(expected = DAOException.class)
	public void testSaveException() throws DAOException {
		Tag expectedTag = new Tag(1L, "third tag ggggggggggggggggggggggggggggggggggggggggggggg");
		tagDAO.save(expectedTag);
	}
}