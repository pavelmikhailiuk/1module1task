package by.epam.newsdao.dao.impl;

import static org.junit.Assert.*;

import java.sql.Timestamp;
import java.text.ParseException;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.*;

import by.epam.newsdao.dao.AuthorDAO;
import by.epam.newsdao.entity.Author;
import by.epam.newsdao.exception.DAOException;

/**
 * The Class AuthorDAOTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({ "classpath:spring-testconfig.xml" })
@DbUnitConfiguration(databaseConnection = "dbUnitDatabaseConnection")
@DatabaseSetup(value = "/dbunitAuthorData.xml")
@DatabaseTearDown(value = "/dbunitAuthorData.xml", type = DatabaseOperation.DELETE_ALL)
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class, DbUnitTestExecutionListener.class })
public class AuthorDAOTest {

	/** The author DAO. */
	@Autowired
	private AuthorDAO authorDAO;

	/**
	 * Test find.
	 *
	 * @throws Exception
	 *             the DAO exception if database operation failed
	 */
	@Test
	public void testFind() throws DAOException {
		Author expectedAuthor = new Author(1L, "john doe", Timestamp.valueOf("2015-08-10 12:46:29.77"));
		Author actualAuthor = authorDAO.find(1L);
		assertEquals(actualAuthor, expectedAuthor);
	}

	/**
	 * Test get all.
	 *
	 * @throws Exception
	 *             the DAO exception if database operation failed
	 */
	@Test
	public void testGetAll() throws DAOException {
		List<Author> authorsList = authorDAO.findAll();
		assertEquals(authorsList.size(), 2);
	}

	/**
	 * Test save.
	 *
	 * @throws Exception
	 *             the DAO exception if database operation failed
	 */
	@Test
	public void testSave() throws DAOException {
		Author expectedAuthor = new Author(111L, "Jimmy", null);
		long id = authorDAO.save(expectedAuthor);
		expectedAuthor.setAuthorId(id);
		Author actualAuthor = authorDAO.find(id);
		assertEquals(actualAuthor, expectedAuthor);
	}

	/**
	 * Test update.
	 * 
	 * @throws ParseException
	 *
	 * @throws Exception
	 *             the DAO exception if database operation failed
	 */
	@Test
	public void testUpdate() throws DAOException, ParseException {
		Author expectedAuthor = new Author(1L, "NewJimmy", Timestamp.valueOf("2015-08-10 12:46:29.77"));
		authorDAO.update(expectedAuthor);
		Author actualAuthor = authorDAO.find(1L);
		assertEquals(actualAuthor, expectedAuthor);
	}

	/**
	 * Test delete.
	 *
	 * @throws DAO
	 *             exception database operation failed
	 */
	@Test
	public void testDelete() throws DAOException {
		Author actualAuthor = authorDAO.find(2L);
		assertNotNull(actualAuthor);
		authorDAO.delete(2L);
		actualAuthor = authorDAO.find(2L);
		assertNull(actualAuthor);
	}

	/**
	 * Test update exception.
	 *
	 * @throws DAO
	 *             exception database operation failed
	 */
	@Test(expected = DAOException.class)
	public void testUpdateException() throws DAOException {
		Author expectedAuthor = new Author(1L, "NewJimmyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy",
				Timestamp.valueOf("2015-08-10 12:46:29.77"));
		authorDAO.update(expectedAuthor);
	}

	/**
	 * Test save exception.
	 *
	 * @throws DAO
	 *             exception database operation failed
	 */
	@Test(expected = DAOException.class)
	public void testSaveException() throws DAOException {
		Author expectedAuthor = new Author(1L, "NewJimmyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy",
				Timestamp.valueOf("2015-08-10 12:46:29.77"));
		authorDAO.save(expectedAuthor);
	}
}