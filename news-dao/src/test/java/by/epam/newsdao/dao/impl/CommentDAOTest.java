package by.epam.newsdao.dao.impl;

import static org.junit.Assert.*;

import java.sql.Timestamp;
import java.text.ParseException;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.*;

import by.epam.newsdao.dao.CommentDAO;
import by.epam.newsdao.entity.Comment;
import by.epam.newsdao.exception.DAOException;

/**
 * The Class CommentDAOTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({ "classpath:spring-testconfig.xml" })
@DbUnitConfiguration(databaseConnection = "dbUnitDatabaseConnection")
@DatabaseSetup(value = "/dbunitCommentData.xml")
@DatabaseTearDown(value = "/dbunitCommentData.xml", type = DatabaseOperation.DELETE_ALL)
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class, DbUnitTestExecutionListener.class })
public class CommentDAOTest {

	/** The comment DAO. */
	@Autowired
	private CommentDAO commentDAO;

	/**
	 * Test find.
	 *
	 * @throws Exception
	 *             the DAO exception if database operation failed
	 */
	@Test
	public void testFind() throws DAOException {
		Comment expectedComment = new Comment(1L, 1L, "first comment", Timestamp.valueOf("2015-08-10 12:46:29.77"));
		Comment actualComment = commentDAO.find(1L);
		assertEquals(actualComment, expectedComment);
	}

	/**
	 * Test get all.
	 *
	 * @throws Exception
	 *             the DAO exception if database operation failed
	 */
	@Test
	public void testGetAll() throws DAOException {
		List<Comment> commentList = commentDAO.findAll();
		assertEquals(commentList.size(), 2);
	}

	/**
	 * Test save.
	 * 
	 * @throws ParseException
	 *
	 * @throws Exception
	 *             the DAO exception if database operation failed
	 */
	@Test
	public void testSave() throws DAOException, ParseException {
		Comment expectedComment = new Comment(3L, 1L, "comment text", Timestamp.valueOf("2015-08-10 12:46:29.77"));
		long commentId = commentDAO.save(expectedComment);
		expectedComment.setCommentId(commentId);
		Comment actualComment = commentDAO.find(commentId);
		assertEquals(actualComment, expectedComment);
	}

	/**
	 * Test update.
	 *
	 * @throws Exception
	 *             the DAO exception if database operation failed
	 */
	@Test
	public void testUpdate() throws DAOException {
		Comment expectedComment = new Comment(1L, 1L, "new comment text", Timestamp.valueOf("2015-08-10 12:46:29.77"));
		commentDAO.update(expectedComment);
		Comment actualComment = commentDAO.find(1L);
		assertEquals(actualComment, expectedComment);
	}

	/**
	 * Test delete.
	 *
	 * @throws Exception
	 *             the DAO exception if database operation failed
	 */
	@Test
	public void testDelete() throws DAOException {
		Comment actualComment = commentDAO.find(1L);
		assertNotNull(actualComment);
		commentDAO.delete(1L);
		actualComment = commentDAO.find(1L);
		assertNull(actualComment);
	}

	/**
	 * Test delete by news id.
	 *
	 * @throws Exception
	 *             the DAO exception if database operation failed
	 */
	@Test
	public void testDeleteByNewsId() throws DAOException {
		Comment actualComment = commentDAO.find(2L);
		assertNotNull(actualComment);
		Long[] newsId = new Long[] { 2L };
		commentDAO.deleteByNewsId(newsId);
		actualComment = commentDAO.find(2L);
		assertNull(actualComment);
	}

	/**
	 * Test update exception.
	 *
	 * @throws DAO
	 *             exception database operation failed
	 */
	@Test(expected = DAOException.class)
	public void testUpdateException() throws DAOException {
		Comment expectedComment = new Comment(1L, 1L,
				"comment text tttttttttttttttttttttttttttttt ttttttttttttt ttttttttttttttttttttttttttttt vvvvvvvvvvvvvv",
				Timestamp.valueOf("2015-08-10 12:46:29.77"));
		commentDAO.update(expectedComment);
	}

	/**
	 * Test save exception.
	 *
	 * @throws DAO
	 *             exception database operation failed
	 */
	@Test(expected = DAOException.class)
	public void testSaveException() throws DAOException {
		Comment expectedComment = new Comment(150L, 230L, "comment text", Timestamp.valueOf("2015-08-10 12:46:29.77"));
		commentDAO.save(expectedComment);
	}
}