package by.epam.newsdao.entity;

import java.io.Serializable;
import java.util.List;

/**
 * The Class SearchCriteria. Search criteria object.
 */
public class SearchCriteria implements Serializable {
	
	/** The Constant serialVersionUID. */
	public static final long serialVersionUID = 1L;

	/** The author. */
	private Author author;
	
	/** The tag list. */
	private List<Tag> tagList;

	/**
	 * Instantiates a new search criteria.
	 *
	 * @param author the author object
	 * @param tagList the list of tag object
	 */
	public SearchCriteria(Author author, List<Tag> tagList) {
		this.author = author;
		this.tagList = tagList;
	}

	/**
	 * Instantiates a new search criteria.
	 */
	public SearchCriteria() {
	}

	/**
	 * Gets the author.
	 *
	 * @return the author
	 */
	public Author getAuthor() {
		return author;
	}

	/**
	 * Sets the author.
	 *
	 * @param author the new author
	 */
	public void setAuthor(Author author) {
		this.author = author;
	}

	/**
	 * Gets the tag list.
	 *
	 * @return the tag list
	 */
	public List<Tag> getTagList() {
		return tagList;
	}

	/**
	 * Sets the tag list.
	 *
	 * @param tagList the new tag list
	 */
	public void setTagList(List<Tag> tagList) {
		this.tagList = tagList;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		int result = 31 + ((author == null) ? 0 : author.hashCode());
		result = 31 * result + ((tagList == null) ? 0 : tagList.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if ((obj == null) || (getClass() != obj.getClass()))
			return false;
		SearchCriteria other = (SearchCriteria) obj;
		if (author == null) {
			if (other.author != null)
				return false;
		} else if (!author.equals(other.author))
			return false;
		if (tagList == null) {
			if (other.tagList != null)
				return false;
		} else if (!tagList.equals(other.tagList))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "SearchCriteria [author=" + author + ", tagList=" + tagList
				+ "]";
	}

}
