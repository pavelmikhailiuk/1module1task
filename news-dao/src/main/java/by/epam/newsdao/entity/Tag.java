package by.epam.newsdao.entity;

import java.io.Serializable;

import javax.validation.constraints.Size;

/**
 * The Class Tag.
 */
public class Tag implements Serializable {

	/** The Constant serialVersionUID. */
	public static final long serialVersionUID = 1L;

	/** The tag id. */
	private Long tagId;

	/** The tag name. */
	@Size(min = 2, max = 30)
	private String tagName;

	/**
	 * Instantiates a new tag.
	 */
	public Tag() {
	}

	/**
	 * Instantiates a new tag.
	 *
	 * @param tagId
	 *            the tag id
	 * @param tagName
	 *            the tag name
	 */
	public Tag(Long tagId, String tagName) {
		this.tagId = tagId;
		this.tagName = tagName;
	}

	/**
	 * Gets the tag id.
	 *
	 * @return the tag id
	 */
	public Long getTagId() {
		return tagId;
	}

	/**
	 * Sets the tag id.
	 *
	 * @param tagId
	 *            the new tag id
	 */
	public void setTagId(Long tagId) {
		this.tagId = tagId;
	}

	/**
	 * Gets the tag name.
	 *
	 * @return the tag name
	 */
	public String getTagName() {
		return tagName;
	}

	/**
	 * Sets the tag name.
	 *
	 * @param tagName
	 *            the new tag name
	 */
	public void setTagName(String tagName) {
		this.tagName = tagName;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		int result = 31 + ((tagId == null) ? 0 : tagId.hashCode());
		result = 31 * result + ((tagName == null) ? 0 : tagName.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if ((obj == null) || (getClass() != obj.getClass()))
			return false;
		Tag other = (Tag) obj;
		if (tagId == null) {
			if (other.tagId != null)
				return false;
		} else if (!tagId.equals(other.tagId))
			return false;
		if (tagName == null) {
			if (other.tagName != null)
				return false;
		} else if (!tagName.equals(other.tagName))
			return false;
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Tag [tagId=" + tagId + ", tagName=" + tagName + "]";
	}

}
