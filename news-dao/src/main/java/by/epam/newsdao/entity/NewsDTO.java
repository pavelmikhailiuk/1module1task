package by.epam.newsdao.entity;

import java.io.Serializable;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * The Class NewsDTO. Data transfer object.
 */
public class NewsDTO implements Serializable {
	
	/** The Constant serialVersionUID. */
	public static final long serialVersionUID = 1L;

	/** The news. */
	@NotNull
	@Valid
	private News news;
	
	/** The author. */
	@NotNull
	private Author author;
	
	/** The tags. */
	@NotNull
	private List<Tag> tags;
	
	/** The comments. */
	private List<Comment> comments;

	/**
	 * Instantiates a new news DTO.
	 */
	public NewsDTO() {
	}

	/**
	 * Instantiates a new news DTO.
	 *
	 * @param news the news object
	 * @param author the author object
	 * @param tags the  list of tag objects
	 * @param comments the list of comment objects
	 */
	public NewsDTO(News news, Author author, List<Tag> tags,
			List<Comment> comments) {
		super();
		this.news = news;
		this.author = author;
		this.tags = tags;
		this.comments = comments;
	}

	/**
	 * Gets the news.
	 *
	 * @return the news
	 */
	public News getNews() {
		return news;
	}

	/**
	 * Sets the news.
	 *
	 * @param news the new news
	 */
	public void setNews(News news) {
		this.news = news;
	}

	/**
	 * Gets the author.
	 *
	 * @return the author
	 */
	public Author getAuthor() {
		return author;
	}

	/**
	 * Sets the author.
	 *
	 * @param author the new author
	 */
	public void setAuthor(Author author) {
		this.author = author;
	}

	/**
	 * Gets the tags.
	 *
	 * @return the tags
	 */
	public List<Tag> getTags() {
		return tags;
	}

	/**
	 * Sets the tags.
	 *
	 * @param tags the new tags
	 */
	public void setTags(List<Tag> tags) {
		this.tags = tags;
	}

	/**
	 * Gets the comments.
	 *
	 * @return the comments
	 */
	public List<Comment> getComments() {
		return comments;
	}

	/**
	 * Sets the comments.
	 *
	 * @param comments the new comments
	 */
	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		int result = 31 + ((author == null) ? 0 : author.hashCode());
		result = 31 * result + ((comments == null) ? 0 : comments.hashCode());
		result = 31 * result + ((news == null) ? 0 : news.hashCode());
		result = 31 * result + ((tags == null) ? 0 : tags.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if ((obj == null) || (getClass() != obj.getClass()))
			return false;
		NewsDTO other = (NewsDTO) obj;
		if (author == null) {
			if (other.author != null)
				return false;
		} else if (!author.equals(other.author))
			return false;
		if (comments == null) {
			if (other.comments != null)
				return false;
		} else if (!comments.equals(other.comments))
			return false;
		if (news == null) {
			if (other.news != null)
				return false;
		} else if (!news.equals(other.news))
			return false;
		if (tags == null) {
			if (other.tags != null)
				return false;
		} else if (!tags.equals(other.tags))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "NewsDTO [news=" + news + ", author=" + author + ", tags="
				+ tags + ", comments=" + comments + "]";
	}
}
