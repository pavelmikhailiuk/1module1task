package by.epam.newsdao.entity;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

/**
 * The Class Author.
 */
public class Author implements Serializable {

	/** The Constant serialVersionUID. */
	public static final long serialVersionUID = 1L;

	/** The author id. */
	private Long authorId;

	/** The author name. */
	@Size(min = 2, max = 30)
	@Pattern(regexp = "^([A-Z][a-z]+([ ]+[A-Z][a-z]+)?)$|^([�-ߨ][�-��]+([ ]+[�-ߨ][�-��]+)?)$")
	private String authorName;

	/** The expired. */
	private Date expired;

	/**
	 * Instantiates a new author.
	 */
	public Author() {
	}

	/**
	 * Instantiates a new author.
	 *
	 * @param authorId
	 *            the author id
	 * @param authorName
	 *            the author name
	 * @param expired
	 *            the expired Date object
	 */
	public Author(Long authorId, String authorName, Date expired) {
		this.authorId = authorId;
		this.authorName = authorName;
		this.expired = expired;
	}

	/**
	 * Gets the author id.
	 *
	 * @return the author id
	 */
	public Long getAuthorId() {
		return authorId;
	}

	/**
	 * Sets the author id.
	 *
	 * @param authorId
	 *            the new author id
	 */
	public void setAuthorId(Long authorId) {
		this.authorId = authorId;
	}

	/**
	 * Gets the author name.
	 *
	 * @return the author name
	 */
	public String getAuthorName() {
		return authorName;
	}

	/**
	 * Sets the author name.
	 *
	 * @param authorName
	 *            the new author name
	 */
	public void setAuthorName(String authorName) {
		this.authorName = authorName;
	}

	/**
	 * Gets the expired.
	 *
	 * @return the expired
	 */
	public Date getExpired() {
		return expired;
	}

	/**
	 * Sets the expired.
	 *
	 * @param expired
	 *            the new expired
	 */
	public void setExpired(Date expired) {
		this.expired = expired;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		int result = 31 + ((authorId == null) ? 0 : authorId.hashCode());
		result = 31 * result
				+ ((authorName == null) ? 0 : authorName.hashCode());
		result = 31 * result + ((expired == null) ? 0 : expired.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if ((obj == null) || (getClass() != obj.getClass()))
			return false;
		Author other = (Author) obj;
		if (authorId == null) {
			if (other.authorId != null)
				return false;
		} else if (!authorId.equals(other.authorId))
			return false;
		if (authorName == null) {
			if (other.authorName != null)
				return false;
		} else if (!authorName.equals(other.authorName))
			return false;
		if (expired == null) {
			if (other.expired != null)
				return false;
		} else if (!expired.equals(other.expired))
			return false;
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Author [authorId=" + authorId + ", authorName=" + authorName
				+ ", expired="+expired+"]";
	}

}
