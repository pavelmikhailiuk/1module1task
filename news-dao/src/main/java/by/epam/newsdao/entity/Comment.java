package by.epam.newsdao.entity;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.Size;

/**
 * The Class Comment.
 */
public class Comment implements Serializable {

	/** The Constant serialVersionUID. */
	public static final long serialVersionUID = 1L;

	/** The comment id. */
	private Long commentId;

	/** The news id. */
	private Long newsId;

	/** The comment text. */
	@Size(min = 2, max = 100)
	private String commentText;

	/** The creation date. */
	private Date creationDate;

	/**
	 * Instantiates a new comment.
	 */
	public Comment() {
	}

	/**
	 * Instantiates a new comment.
	 *
	 * @param commentId
	 *            the comment id
	 * @param newsId
	 *            the news id
	 * @param commentText
	 *            the comment text
	 * @param creationDate
	 *            the creation date Date object
	 */
	public Comment(Long commentId, Long newsId, String commentText,
			Date creationDate) {
		this.commentId = commentId;
		this.newsId = newsId;
		this.commentText = commentText;
		this.creationDate = creationDate;
	}

	/**
	 * Gets the comment id.
	 *
	 * @return the comment id
	 */
	public Long getCommentId() {
		return commentId;
	}

	/**
	 * Sets the comment id.
	 *
	 * @param commentId
	 *            the new comment id
	 */
	public void setCommentId(Long commentId) {
		this.commentId = commentId;
	}

	/**
	 * Gets the news id.
	 *
	 * @return the news id
	 */
	public Long getNewsId() {
		return newsId;
	}

	/**
	 * Sets the news id.
	 *
	 * @param newsId
	 *            the new news id
	 */
	public void setNewsId(Long newsId) {
		this.newsId = newsId;
	}

	/**
	 * Gets the comment text.
	 *
	 * @return the comment text
	 */
	public String getCommentText() {
		return commentText;
	}

	/**
	 * Sets the comment text.
	 *
	 * @param commentText
	 *            the new comment text
	 */
	public void setCommentText(String commentText) {
		this.commentText = commentText;
	}

	/**
	 * Gets the creation date.
	 *
	 * @return the creation date
	 */
	public Date getCreationDate() {
		return creationDate;
	}

	/**
	 * Sets the creation date.
	 *
	 * @param creationDate
	 *            the new creation date
	 */
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		int result = 31 + ((commentId == null) ? 0 : commentId.hashCode());
		result = 31 * result
				+ ((commentText == null) ? 0 : commentText.hashCode());
		result = 31 * result
				+ ((creationDate == null) ? 0 : creationDate.hashCode());
		result = 31 * result + ((newsId == null) ? 0 : newsId.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if ((obj == null) || (getClass() != obj.getClass()))
			return false;
		Comment other = (Comment) obj;
		if (commentId == null) {
			if (other.commentId != null)
				return false;
		} else if (!commentId.equals(other.commentId))
			return false;
		if (commentText == null) {
			if (other.commentText != null)
				return false;
		} else if (!commentText.equals(other.commentText))
			return false;
		if (creationDate == null) {
			if (other.creationDate != null)
				return false;
		} else if (!creationDate.equals(other.creationDate))
			return false;
		if (newsId == null) {
			if (other.newsId != null)
				return false;
		} else if (!newsId.equals(other.newsId))
			return false;
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Comment [commentId=" + commentId + ", newsId=" + newsId
				+ ", commentText=" + commentText + ", creationDate="
				+ creationDate + "]";
	}

}
