package by.epam.newsdao.dao.impl;

import static by.epam.newsdao.constant.TablesFieldsConstants.*;

import java.sql.*;
import java.util.*;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Repository;

import by.epam.newsdao.dao.AuthorDAO;
import by.epam.newsdao.dao.util.DaoUtils;
import by.epam.newsdao.entity.Author;
import by.epam.newsdao.exception.DAOException;

// TODO: Auto-generated Javadoc
/**
 * The Class AuthorDAOImpl. Implements interface AuthorDAO
 */
@Repository
public class AuthorDAOImpl implements AuthorDAO {

	/** The Constant UTC. */
	private static final String UTC = "UTC";
	/** The Constant SQL_FIND_ALL. */
	private static final String SQL_FIND_ALL = "select author_id, author_name, expired from Author";

	/** The Constant SQL_DELETE. */
	private static final String SQL_DELETE = "delete from Author where author_id=?";

	/** The Constant SQL_UPDATE. */
	private static final String SQL_UPDATE = "update Author set author_name=?, expired=? where author_id = ?";

	/** The Constant SQL_FIND. */
	private static final String SQL_FIND = "select author_id, author_name, expired from Author where author_id = ?";

	/** The Constant SQL_SAVE. */
	private static final String SQL_SAVE = "insert into Author (author_name) values (?)";

	/** The data source. */
	@Autowired
	private DataSource dataSource;

	/** The dao utils. */
	@Autowired
	private DaoUtils daoUtils;

	/*
	 * (non-Javadoc)
	 * 
	 * @see by.epam.newsmanagement.dao.CRUDDAO#save(java.lang.Object)
	 */
	@Override
	public Long save(Author author) throws DAOException {
		Long id = null;
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_SAVE, new String[] { AUTHOR_ID })) {
			preparedStatement.setString(1, author.getAuthorName());
			preparedStatement.executeUpdate();
			ResultSet tableKeys = preparedStatement.getGeneratedKeys();
			tableKeys.next();
			id = tableKeys.getLong(1);
		} catch (SQLException e) {
			throw new DAOException("Error save author", e);
		} finally {
			daoUtils.releaseConnection(connection, dataSource);
		}
		return id;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see by.epam.newsmanagement.dao.CRUDDAO#find(java.lang.Long)
	 */
	@Override
	public Author find(Long id) throws DAOException {
		Author author = null;
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_FIND)) {
			preparedStatement.setLong(1, id);
			ResultSet resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				author = initAuthor(resultSet);
			}
		} catch (SQLException e) {
			throw new DAOException("Error find author by id", e);
		} finally {
			daoUtils.releaseConnection(connection, dataSource);
		}
		return author;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see by.epam.newsmanagement.dao.CRUDDAO#update(java.lang.Object)
	 */
	@Override
	public void update(Author author) throws DAOException {
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_UPDATE)) {
			preparedStatement.setString(1, author.getAuthorName());
			if (author.getExpired() != null) {
				Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone(UTC));
				preparedStatement.setTimestamp(2, new Timestamp(author.getExpired().getTime()), calendar);
			} else {
				preparedStatement.setTimestamp(2, null);
			}
			preparedStatement.setLong(3, author.getAuthorId());
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException("Error update author", e);
		} finally {
			daoUtils.releaseConnection(connection, dataSource);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see by.epam.newsmanagement.dao.CRUDDAO#delete(java.lang.Long)
	 */
	@Override
	public void delete(Long id) throws DAOException {
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_DELETE)) {
			preparedStatement.setLong(1, id);
			preparedStatement.executeQuery();
		} catch (SQLException e) {
			throw new DAOException("Error delete author by id", e);
		} finally {
			daoUtils.releaseConnection(connection, dataSource);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see by.epam.newsmanagement.dao.CRUDDAO#findAll()
	 */
	@Override
	public List<Author> findAll() throws DAOException {
		List<Author> authorList = new ArrayList<>();
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_FIND_ALL)) {
			ResultSet resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				authorList.add(initAuthor(resultSet));
			}
		} catch (SQLException e) {
			throw new DAOException("Error find all authors", e);
		} finally {
			daoUtils.releaseConnection(connection, dataSource);
		}
		return authorList;
	}

	/**
	 * Inits the author.
	 *
	 * @param rs
	 *            the resultset
	 * @return the author
	 * @throws DAOException
	 *             the DAO exception if operation failed
	 */
	private Author initAuthor(ResultSet rs) throws DAOException {
		Author author = new Author();
		Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone(UTC));
		try {
			author.setAuthorId(rs.getLong(AUTHOR_ID));
			author.setAuthorName(rs.getString(AUTHOR_NAME));
			author.setExpired(rs.getTimestamp(EXPIRED, calendar));
		} catch (SQLException e) {
			throw new DAOException("Error author initialization", e);
		}
		return author;
	}
}
