package by.epam.newsdao.dao.impl;

import static by.epam.newsdao.constant.TablesFieldsConstants.*;

import java.sql.*;
import java.util.*;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Repository;

import by.epam.newsdao.dao.CommentDAO;
import by.epam.newsdao.dao.util.DaoUtils;
import by.epam.newsdao.dao.util.SubQueryCreator;
import by.epam.newsdao.entity.Comment;
import by.epam.newsdao.exception.DAOException;

/**
 * The Class CommentDAOImpl. Implements interface CommentDAO
 */
@Repository
public class CommentDAOImpl implements CommentDAO {

	private static final String UTC = "UTC";

	/** The Constant SQL_FIND_ALL. */
	private static final String SQL_FIND_ALL = "SELECT comment_id, news_id, comment_text, creation_date FROM Comments";

	/** The Constant SQL_DELETE_BY_NEWS_ID. */
	private static final String SQL_DELETE_BY_NEWS_ID = "delete from Comments where news_id";

	/** The Constant SQL_DELETE_BY_COMMENT_ID. */
	private static final String SQL_DELETE_BY_COMMENT_ID = "delete from Comments where comment_id=?";

	/** The Constant SQL_UPDATE. */
	private static final String SQL_UPDATE = "update Comments set news_id=?, comment_text=? WHERE comment_ID = ?";

	/** The Constant SQL_FIND. */
	private static final String SQL_FIND = "SELECT comment_id, news_id, comment_text, creation_date FROM Comments WHERE comment_ID = ?";

	/** The Constant SQL_SAVE. */
	private static final String SQL_SAVE = "insert into Comments (news_id, comment_text, creation_date) values (?,?,?)";

	/** The data source. */
	@Autowired
	private DataSource dataSource;

	/** The sub query creator. */
	@Autowired
	private SubQueryCreator creator;
	
	/** The dao utils. */
	@Autowired
	private DaoUtils daoUtils;
	/*
	 * (non-Javadoc)
	 * 
	 * @see by.epam.newsmanagement.dao.CRUDDAO#save(java.lang.Object)
	 */
	@Override
	public Long save(Comment comment) throws DAOException {
		Long id = null;
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_SAVE, new String[] { COMMENT_ID })) {
			preparedStatement.setLong(1, comment.getNewsId());
			preparedStatement.setString(2, comment.getCommentText());
			Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone(UTC));
			preparedStatement.setTimestamp(3, new Timestamp(comment.getCreationDate().getTime()), calendar);
			preparedStatement.executeUpdate();
			ResultSet tableKeys = preparedStatement.getGeneratedKeys();
			tableKeys.next();
			id = tableKeys.getLong(1);
		} catch (SQLException e) {
			throw new DAOException("Error save comment", e);
		} finally {
			daoUtils.releaseConnection(connection, dataSource);
		}
		return id;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see by.epam.newsmanagement.dao.CRUDDAO#find(java.lang.Long)
	 */
	@Override
	public Comment find(Long id) throws DAOException {
		Comment comment = null;
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_FIND)) {
			preparedStatement.setLong(1, id);
			ResultSet resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				comment = initComment(resultSet);
			}
		} catch (SQLException e) {
			throw new DAOException("Error find comment by id", e);
		} finally {
			daoUtils.releaseConnection(connection, dataSource);
		}
		return comment;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see by.epam.newsmanagement.dao.CRUDDAO#update(java.lang.Object)
	 */
	@Override
	public void update(Comment comment) throws DAOException {
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_UPDATE)) {
			preparedStatement.setLong(1, comment.getNewsId());
			preparedStatement.setString(2, comment.getCommentText());
			preparedStatement.setLong(3, comment.getCommentId());
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException("Error update comment", e);
		} finally {
			daoUtils.releaseConnection(connection, dataSource);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see by.epam.newsmanagement.dao.CRUDDAO#delete(java.lang.Long)
	 */
	@Override
	public void delete(Long id) throws DAOException {
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_DELETE_BY_COMMENT_ID)) {
			preparedStatement.setLong(1, id);
			preparedStatement.executeQuery();
		} catch (SQLException e) {
			throw new DAOException("Error delete comment by id", e);
		} finally {
			daoUtils.releaseConnection(connection, dataSource);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see by.epam.newsmanagement.dao.CommentDAO#deleteByNewsId(java.lang.Long)
	 */
	@Override
	public void deleteByNewsId(Long[] id) throws DAOException {
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement preparedStatement = connection
				.prepareStatement(SQL_DELETE_BY_NEWS_ID + creator.createIdSubQuery(id))) {
			preparedStatement.executeQuery();
		} catch (SQLException e) {
			throw new DAOException("Error delete comment by news id", e);
		} finally {
			daoUtils.releaseConnection(connection, dataSource);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see by.epam.newsmanagement.dao.CRUDDAO#findAll()
	 */
	@Override
	public List<Comment> findAll() throws DAOException {
		List<Comment> commentList = new ArrayList<>();
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_FIND_ALL)) {
			ResultSet resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				commentList.add(initComment(resultSet));
			}
		} catch (SQLException e) {
			throw new DAOException("Error find all comments", e);
		} finally {
			daoUtils.releaseConnection(connection, dataSource);
		}
		return commentList;
	}

	/**
	 * Inits the comment.
	 *
	 * @param rs
	 *            the resultset
	 * @return the comment
	 * @throws DAOException
	 *             the DAO exception if operation failed
	 */
	private Comment initComment(ResultSet rs) throws DAOException {
		Comment comment = new Comment();
		Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone(UTC));
		try {
			comment.setCommentId(rs.getLong(COMMENT_ID));
			comment.setNewsId(rs.getLong(NEWS_ID));
			comment.setCommentText(rs.getString(COMMENT_TEXT));
			comment.setCreationDate(rs.getTimestamp(CREATION_DATE, calendar));
		} catch (SQLException e) {
			throw new DAOException("Error comment initialization", e);
		}
		return comment;
	}
}
