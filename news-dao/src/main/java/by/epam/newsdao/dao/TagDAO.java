package by.epam.newsdao.dao;

import by.epam.newsdao.entity.Tag;

/**
 * The Interface TagDAO. Extends interface CRUDDAO
 */
public interface TagDAO extends CRUDDAO<Tag> {

}
