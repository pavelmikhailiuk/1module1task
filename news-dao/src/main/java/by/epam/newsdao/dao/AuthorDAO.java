package by.epam.newsdao.dao;

import by.epam.newsdao.entity.Author;

/**
 * The Interface AuthorDAO. Extends interface CRUDDAO
 */
public interface AuthorDAO extends CRUDDAO<Author> {

}
