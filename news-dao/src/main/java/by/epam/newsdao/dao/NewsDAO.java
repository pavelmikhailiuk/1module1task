package by.epam.newsdao.dao;

import java.util.List;

import by.epam.newsdao.entity.News;
import by.epam.newsdao.entity.SearchCriteria;
import by.epam.newsdao.exception.DAOException;

/**
 * The Interface NewsDAO. Extends interface CRUDDAO
 */
public interface NewsDAO extends CRUDDAO<News> {
	
	/**
	 * Find news by search criteria.
	 *
	 * @param sc the search criteria object
	 * @return the list of news
	 * @throws DAOException the DAO exception if operation failed
	 */
	List<News> findBySearchCriteria(SearchCriteria sc,int offset, int newsAtPage) throws DAOException;

	/**
	 * News count (all).
	 *
	 * @return the long news count
	 * @throws DAOException the DAO exception if operation failed
	 */
	Long newsCount() throws DAOException;

	/**
	 * News count (search results).
	 *
	 * @param sc the the search criteria object
	 * @return the long
	 * @throws DAOException the DAO exception if operation failed
	 */
	Long newsCount(SearchCriteria sc) throws DAOException; 
	
	
	/**
	 * Save news author.
	 *
	 * @param newsId the news id
	 * @param authorId the author id
	 * @throws DAOException the DAO exception if operation failed
	 */
	void saveNewsAuthor(Long newsId, Long authorId) throws DAOException;

	/**
	 * Update news author.
	 *
	 * @param newsId the news id
	 * @param authorId the author id
	 * @throws DAOException the DAO exception if operation failed
	 */
	void updateNewsAuthor(Long newsId, Long authorId) throws DAOException;

	/**
	 * Delete news author.
	 *
	 * @param newsId the news id
	 * @throws DAOException the DAO exception if operation failed
	 */
	void deleteNewsAuthor(Long[] newsId) throws DAOException;

	/**
	 * Save news tag.
	 *
	 * @param newsId the news id
	 * @param tagId the tag id
	 * @throws DAOException the DAO exception if operation failed
	 */
	void saveNewsTag(Long newsId, Long tagId) throws DAOException;

	/**
	 * Update news tag.
	 *
	 * @param newsId the news id
	 * @param tagId the tag id
	 * @throws DAOException the DAO exception if operation failed
	 */
	void updateNewsTag(Long newsId, Long tagId) throws DAOException;

	/**
	 * Delete news tag.
	 *
	 * @param newsId the news id
	 * @throws DAOException the DAO exception if operation failed
	 */
	void deleteNewsTagByNewsId(Long[] newsId) throws DAOException;
	
	/**
	 * Delete news tag.
	 *
	 * @param tagId the tag id
	 * @throws DAOException the DAO exception if operation failed
	 */
	void deleteNewsTagByTagId(Long tagId) throws DAOException;

	/**
	 * Find author id by news id.
	 *
	 * @param newsId the news id
	 * @return the long author id
	 * @throws DAOException the DAO exception if operation failed
	 */
	Long findAuthorIdByNewsId(Long newsId) throws DAOException;

	/**
	 * Find tag id by news id.
	 *
	 * @param newsId the news id
	 * @return the list of long tag id
	 * @throws DAOException the DAO exception if operation failed
	 */
	List<Long> findTagIdByNewsId(Long newsId) throws DAOException;
	
	/**
	 * Find all.
	 *
	 * @return the list of the generic type
	 * @throws DAOException the DAO exception if operation failed
	 */
	List<News> findAll(int offset, int newsAtPage) throws DAOException;
	/**
	 * Delete.
	 *
	 * @param id the array of the news id
	 * @throws DAOException the DAO exception if operation failed
	 */
	void delete(Long[] id) throws DAOException;
}
