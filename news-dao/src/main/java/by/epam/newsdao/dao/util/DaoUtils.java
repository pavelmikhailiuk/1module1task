package by.epam.newsdao.dao.util;

import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Component;

/**
 * The Class DaoUtils.
 */
@Component
public class DaoUtils {
	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(DaoUtils.class);

	/**
	 * Release connection.
	 *
	 * @param connection the connection
	 * @param dataSource the data source
	 */
	public void releaseConnection(Connection connection, DataSource dataSource) {
		try {
			DataSourceUtils.doReleaseConnection(connection, dataSource);
		} catch (SQLException e) {
			LOGGER.error("Error releasing connection", e);
		}
	}
}
