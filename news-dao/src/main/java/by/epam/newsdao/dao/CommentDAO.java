package by.epam.newsdao.dao;

import by.epam.newsdao.entity.Comment;
import by.epam.newsdao.exception.DAOException;


/**
 * The Interface CommentDAO. Extends interface CRUDDAO
 */
public interface CommentDAO extends CRUDDAO<Comment> {
	
	/**
	 * Delete comments by news id.
	 *
	 * @param id the array of the news id
	 * @throws DAOException the DAO exception if operation failed.
	 */
	void deleteByNewsId(Long[] id) throws DAOException;
}
