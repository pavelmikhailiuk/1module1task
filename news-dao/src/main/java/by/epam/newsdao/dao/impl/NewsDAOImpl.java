package by.epam.newsdao.dao.impl;

import static by.epam.newsdao.constant.TablesFieldsConstants.*;

import java.sql.*;
import java.sql.Date;
import java.util.*;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Repository;

import by.epam.newsdao.dao.NewsDAO;
import by.epam.newsdao.dao.util.DaoUtils;
import by.epam.newsdao.dao.util.SubQueryCreator;
import by.epam.newsdao.entity.News;
import by.epam.newsdao.entity.SearchCriteria;
import by.epam.newsdao.exception.DAOException;

/**
 * The Class NewsDAOImpl. Implements interface NewsDAO
 */
@Repository
public class NewsDAOImpl implements NewsDAO {
	private static final String UTC = "UTC";
	/** The Constant SQL_COUNT_NEWS. */
	private static final String SQL_COUNT_NEWS = "select count(*) as rowcount from News";

	/** The Constant SQL_COUNT_FOUND_NEWS. */
	private static final String SQL_COUNT_FOUND_NEWS = "select count(*) as rowcount FROM news left JOIN NEWS_AUTHOR ON news.news_id=NEWS_AUTHOR.NEWS_ID left JOIN NEWS_tag ON news.news_id=NEWS_tag.NEWS_ID where";

	/** The Constant SQL_FIND_ALL. */
	private static final String SQL_FIND_ALL = "SELECT news.news_id, news.title, news.short_text, news.full_text, news.creation_date, news.modification_date, COUNT(comments.news_id) AS commentCount FROM news LEFT JOIN comments ON news.news_id=comments.news_id GROUP BY news.news_id, news.title, news.short_text, news.full_text, news.creation_date, news.modification_date ORDER BY commentCount DESC, news.modification_date DESC";

	/** The Constant SQL_DELETE. */
	private static final String SQL_DELETE = "delete from News where news_id=?";

	/** The Constant SQL_DELETE. */
	private static final String SQL_DELETE_MANY = "delete from News where news_id";

	/** The Constant SQL_FIND_PAGINATION. */
	private static final String SQL_FIND_PAGINATION_PART_1 = "select * from ( select a.*, ROWNUM rnum from (";

	/** The Constant SQL_FIND_PAGINATION. */
	private static final String SQL_FIND_PAGINATION_PART_2 = ") a where ROWNUM <=? ORDER BY commentCount DESC, modification_date DESC) where rnum  > ? ORDER BY commentCount DESC, modification_date DESC";

	/** The Constant SQL_UPDATE. */
	private static final String SQL_UPDATE = "update News set title=?, short_text=?, full_text=?, modification_date=? where news_id = ?";

	/** The Constant SQL_FIND. */
	private static final String SQL_FIND = "SELECT NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE FROM NEWS WHERE NEWS_ID = ?";

	/** The Constant SQL_SAVE. */
	private static final String SQL_SAVE = "insert into News (title, short_text, full_text, creation_date, modification_date) values (?,?,?,?,?)";

	/** The Constant SQL_FIND_COMMON. */
	private static final String SQL_FIND_COMMON = "SELECT news.news_id, news.title, news.short_text, news.full_text, news.creation_date, news.modification_date, COUNT(comments.news_id) AS commentCount FROM news left JOIN NEWS_tag ON news.news_id=NEWS_tag.NEWS_ID left JOIN NEWS_AUTHOR ON news.news_id=NEWS_AUTHOR.NEWS_ID LEFT JOIN comments ON news.news_id=comments.news_id where";

	/** The Constant SQL_SORT. */
	private static final String SQL_SORT = " GROUP BY news.news_id, news.title, news.short_text, news.full_text, news.creation_date, news.modification_date ORDER BY commentCount DESC, news.modification_date DESC";

	/** The Constant SQL_DELETE_NEWS_AUTHOR. */
	private static final String SQL_DELETE_NEWS_AUTHOR = "delete from News_Author where news_id";

	/** The Constant SQL_UPDATE_NEWS_AUTHOR. */
	private static final String SQL_UPDATE_NEWS_AUTHOR = "update News_Author set author_id=? where news_id = ?";

	/** The Constant SQL_SAVE_NEWS_AUTHOR. */
	private static final String SQL_SAVE_NEWS_AUTHOR = "insert into News_Author (news_id, author_id) values (?,?)";

	/** The Constant SQL_DELETE_NEWS_TAG_BY_NEWS_ID. */
	private static final String SQL_DELETE_NEWS_TAG_BY_NEWS_ID = "delete from News_Tag where news_id";

	/** The Constant SQL_DELETE_NEWS_TAG_BY_TAG_ID. */
	private static final String SQL_DELETE_NEWS_TAG_BY_TAG_ID = "delete from News_Tag where tag_id=?";

	/** The Constant SQL_UPDATE_NEWS_TAG. */
	private static final String SQL_UPDATE_NEWS_TAG = "update News_Tag set tag_id=? where news_id = ?";

	/** The Constant SQL_SAVE_NEWS_TAG. */
	private static final String SQL_SAVE_NEWS_TAG = "insert into News_Tag (news_id, tag_id) values (?,?)";

	/** The Constant SQL_FIND_NEWS_AUTHOR_BY_NEWS_ID. */
	private static final String SQL_FIND_NEWS_AUTHOR_BY_NEWS_ID = "select news_id, author_id from News_Author where news_id = ?";

	/** The Constant SQL_FIND_TAGS_BY_NEWS_ID. */
	private static final String SQL_FIND_TAGS_BY_NEWS_ID = "select news_id, tag_id from News_Tag where news_id = ?";

	/** The data source. */
	@Autowired
	private DataSource dataSource;
	/** The sub query creator. */
	@Autowired
	private SubQueryCreator creator;
	/** The dao utils. */
	@Autowired
	private DaoUtils daoUtils;

	/*
	 * (non-Javadoc)
	 * 
	 * @see by.epam.newsmanagement.dao.CRUDDAO#save(java.lang.Object)
	 */
	@Override
	public Long save(News news) throws DAOException {
		Long id = null;
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_SAVE, new String[] { NEWS_ID })) {
			preparedStatement.setString(1, news.getTitle());
			preparedStatement.setString(2, news.getShortText());
			preparedStatement.setString(3, news.getFullText());
			Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone(UTC));
			preparedStatement.setTimestamp(4, new Timestamp(news.getCreationDate().getTime()), calendar);
			preparedStatement.setDate(5, new Date(news.getModificationDate().getTime()), calendar);
			preparedStatement.executeUpdate();
			ResultSet tableKeys = preparedStatement.getGeneratedKeys();
			tableKeys.next();
			id = tableKeys.getLong(1);
		} catch (SQLException e) {
			throw new DAOException("Error save news", e);
		} finally {
			daoUtils.releaseConnection(connection, dataSource);
		}
		return id;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see by.epam.newsmanagement.dao.CRUDDAO#find(java.lang.Long)
	 */
	@Override
	public News find(Long id) throws DAOException {
		News news = null;
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_FIND)) {
			preparedStatement.setLong(1, id);
			ResultSet resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				news = initNews(resultSet);
			}
		} catch (SQLException e) {
			throw new DAOException("Error find news by id", e);
		} finally {
			daoUtils.releaseConnection(connection, dataSource);
		}
		return news;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see by.epam.newsmanagement.dao.CRUDDAO#update(java.lang.Object)
	 */
	@Override
	public void update(News news) throws DAOException {
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_UPDATE)) {
			preparedStatement.setString(1, news.getTitle());
			preparedStatement.setString(2, news.getShortText());
			preparedStatement.setString(3, news.getFullText());
			Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone(UTC));
			preparedStatement.setDate(4, new Date(news.getModificationDate().getTime()), calendar);
			preparedStatement.setLong(5, news.getNewsId());
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException("Error update news", e);
		} finally {
			daoUtils.releaseConnection(connection, dataSource);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see by.epam.newsmanagement.dao.CRUDDAO#delete(java.lang.Long)
	 */
	@Override
	public void delete(Long id) throws DAOException {
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_DELETE)) {
			preparedStatement.setLong(1, id);
			preparedStatement.executeQuery();
		} catch (SQLException e) {
			throw new DAOException("Error delete news by id", e);
		} finally {
			daoUtils.releaseConnection(connection, dataSource);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see by.epam.newsmanagement.dao.CRUDDAO#findAll()
	 */
	@Override
	public List<News> findAll(int offset, int newsAtPage) throws DAOException {
		List<News> newsList = new ArrayList<>();
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement preparedStatement = connection
				.prepareStatement(SQL_FIND_PAGINATION_PART_1 + SQL_FIND_ALL + SQL_FIND_PAGINATION_PART_2)) {
			preparedStatement.setInt(1, newsAtPage + offset);
			preparedStatement.setInt(2, offset);
			ResultSet resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				newsList.add(initNews(resultSet));
			}
		} catch (SQLException e) {
			throw new DAOException("Error find all news", e);
		} finally {
			daoUtils.releaseConnection(connection, dataSource);
		}
		return newsList;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see by.epam.newsmanagement.dao.CRUDDAO#findAll()
	 */
	@Override
	public List<News> findAll() throws DAOException {
		List<News> newsList = new ArrayList<>();
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_FIND_ALL)) {
			ResultSet resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				newsList.add(initNews(resultSet));
			}
		} catch (SQLException e) {
			throw new DAOException("Error find all news", e);
		} finally {
			daoUtils.releaseConnection(connection, dataSource);
		}
		return newsList;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see by.epam.newsmanagement.dao.NewsDAO#findBySearchCriteria(by.epam.
	 * newsmanagement.entity.SearchCriteria)
	 */
	@Override
	public List<News> findBySearchCriteria(SearchCriteria searchCriteria, int offset, int newsAtPage)
			throws DAOException {
		String query = createSearchSQLRequest(searchCriteria);
		List<News> newsList = new ArrayList<>();
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement preparedStatement = connection
				.prepareStatement(SQL_FIND_PAGINATION_PART_1 + query + SQL_FIND_PAGINATION_PART_2)) {
			preparedStatement.setInt(1, newsAtPage + offset);
			preparedStatement.setInt(2, offset);
			ResultSet resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				newsList.add(initNews(resultSet));
			}
		} catch (SQLException e) {
			throw new DAOException("Error find news by search criteria", e);
		} finally {
			daoUtils.releaseConnection(connection, dataSource);
		}
		return newsList;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see by.epam.newsmanagement.dao.NewsDAO#newsCount()
	 */
	@Override
	public Long newsCount() throws DAOException {
		Long newsCount = null;
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_COUNT_NEWS)) {
			ResultSet resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				newsCount = resultSet.getLong(ROWCOUNT);
			}
		} catch (SQLException e) {
			throw new DAOException("Error count all news", e);
		} finally {
			daoUtils.releaseConnection(connection, dataSource);
		}
		return newsCount;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see by.epam.newsmanagement.dao.NewsDAO#newsCount(by.epam.newsmanagement.
	 * entity .SearchCriteria)
	 */
	@Override
	public Long newsCount(SearchCriteria sc) throws DAOException {
		Long newsCount = null;
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement preparedStatement = connection
				.prepareStatement(SQL_COUNT_FOUND_NEWS + creator.createSearchSubQuery(sc))) {
			ResultSet resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				newsCount = resultSet.getLong(ROWCOUNT);
			}
		} catch (SQLException e) {
			throw new DAOException("Error count news found by search criteria", e);
		} finally {
			daoUtils.releaseConnection(connection, dataSource);
		}
		return newsCount;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see by.epam.newsmanagement.dao.NewsDAO#saveNewsAuthor(java.lang.Long,
	 * java.lang.Long)
	 */
	@Override
	public void saveNewsAuthor(Long newsId, Long authorId) throws DAOException {
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_SAVE_NEWS_AUTHOR)) {
			preparedStatement.setLong(1, newsId);
			preparedStatement.setLong(2, authorId);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException("Error save newsAuthor", e);
		} finally {
			daoUtils.releaseConnection(connection, dataSource);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see by.epam.newsmanagement.dao.NewsDAO#updateNewsAuthor(java.lang.Long,
	 * java.lang.Long)
	 */
	@Override
	public void updateNewsAuthor(Long newsId, Long authorId) throws DAOException {
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_UPDATE_NEWS_AUTHOR)) {
			preparedStatement.setLong(1, authorId);
			preparedStatement.setLong(2, newsId);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException("Error update newsAuthor", e);
		} finally {
			daoUtils.releaseConnection(connection, dataSource);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see by.epam.newsmanagement.dao.NewsDAO#deleteNewsAuthor(java.lang.Long)
	 */
	@Override
	public void deleteNewsAuthor(Long[] newsId) throws DAOException {
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement preparedStatement = connection
				.prepareStatement(SQL_DELETE_NEWS_AUTHOR + creator.createIdSubQuery(newsId))) {
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException("Error delete newsAuthor", e);
		} finally {
			daoUtils.releaseConnection(connection, dataSource);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see by.epam.newsmanagement.dao.NewsDAO#saveNewsTag(java.lang.Long,
	 * java.lang.Long)
	 */
	@Override
	public void saveNewsTag(Long newsId, Long tagId) throws DAOException {
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_SAVE_NEWS_TAG)) {
			preparedStatement.setLong(1, newsId);
			preparedStatement.setLong(2, tagId);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException("Error save newsTag", e);
		} finally {
			daoUtils.releaseConnection(connection, dataSource);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see by.epam.newsmanagement.dao.NewsDAO#delete(java.lang.Long[])
	 */
	@Override
	public void delete(Long[] newsId) throws DAOException {
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement preparedStatement = connection
				.prepareStatement(SQL_DELETE_MANY + creator.createIdSubQuery(newsId))) {
			preparedStatement.executeQuery();
		} catch (SQLException e) {
			throw new DAOException("Error delete news by id", e);
		} finally {
			daoUtils.releaseConnection(connection, dataSource);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * by.epam.newsmanagement.dao.NewsDAO#findAuthorIdByNewsId(java.lang.Long)
	 */
	@Override
	public Long findAuthorIdByNewsId(Long newsId) throws DAOException {
		Long authorId = null;
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_FIND_NEWS_AUTHOR_BY_NEWS_ID)) {
			preparedStatement.setLong(1, newsId);
			ResultSet resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				authorId = resultSet.getLong(AUTHOR_ID);
			}
		} catch (SQLException e) {
			throw new DAOException("Error find author id by news id", e);
		} finally {
			daoUtils.releaseConnection(connection, dataSource);
		}
		return authorId;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see by.epam.newsmanagement.dao.NewsDAO#updateNewsTag(java.lang.Long,
	 * java.lang.Long)
	 */
	@Override
	public void updateNewsTag(Long newsId, Long tagId) throws DAOException {
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_UPDATE_NEWS_TAG)) {
			preparedStatement.setLong(1, tagId);
			preparedStatement.setLong(2, newsId);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException("Error update newsTag", e);
		} finally {
			daoUtils.releaseConnection(connection, dataSource);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see by.epam.newsmanagement.dao.NewsDAO#deleteNewsTag(java.lang.Long)
	 */
	@Override
	public void deleteNewsTagByNewsId(Long[] newsId) throws DAOException {
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement preparedStatement = connection
				.prepareStatement(SQL_DELETE_NEWS_TAG_BY_NEWS_ID + creator.createIdSubQuery(newsId))) {
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException("Error delete newsTag by news id", e);
		} finally {
			daoUtils.releaseConnection(connection, dataSource);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * by.epam.newsmanagement.dao.NewsDAO#deleteNewsTagByTagId(java.lang.Long)
	 */
	@Override
	public void deleteNewsTagByTagId(Long tagId) throws DAOException {
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_DELETE_NEWS_TAG_BY_TAG_ID)) {
			preparedStatement.setLong(1, tagId);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException("Error delete newsTag by tag id", e);
		} finally {
			daoUtils.releaseConnection(connection, dataSource);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see by.epam.newsmanagement.dao.NewsDAO#findTagIdByNewsId(java.lang.Long)
	 */
	@Override
	public List<Long> findTagIdByNewsId(Long newsId) throws DAOException {
		List<Long> tagIdList = new ArrayList<>();
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_FIND_TAGS_BY_NEWS_ID)) {
			preparedStatement.setLong(1, newsId);
			ResultSet resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				tagIdList.add(resultSet.getLong(TAG_ID));
			}
		} catch (SQLException e) {
			throw new DAOException("Error find tags by news id", e);
		} finally {
			daoUtils.releaseConnection(connection, dataSource);
		}
		return tagIdList;
	}

	/**
	 * Inits the news.
	 *
	 * @param rs
	 *            the resultset
	 * @return the news
	 * @throws DAOException
	 *             the DAO exception if operation failed
	 */
	private News initNews(ResultSet rs) throws DAOException {
		News news = new News();
		Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone(UTC));
		try {
			news.setNewsId(rs.getLong(NEWS_ID));
			news.setTitle(rs.getString(TITLE));
			news.setShortText(rs.getString(SHORT_TEXT));
			news.setFullText(rs.getString(FULL_TEXT));
			news.setCreationDate(rs.getTimestamp(CREATION_DATE, calendar));
			news.setModificationDate(rs.getDate(MODIFICATION_DATE, calendar));
		} catch (SQLException e) {
			throw new DAOException("Error news initialization", e);
		}
		return news;
	}

	/**
	 * Creates the SQL query for search by search criteria object.
	 *
	 * @param searchCriteria
	 *            the search criteria object
	 * @return the string SQL query
	 */
	private String createSearchSQLRequest(SearchCriteria searchCriteria) {
		String query = SQL_FIND_ALL;
		query = SQL_FIND_COMMON + creator.createSearchSubQuery(searchCriteria) + SQL_SORT;
		return query;
	}
}
