package by.epam.newsdao.dao.util;

import java.util.List;

import org.springframework.stereotype.Component;

import by.epam.newsdao.entity.*;

/**
 * The Class SubQueryCreator.
 */
@Component
public class SubQueryCreator {

	/**
	 * Creates the SQL sub query from array of id.
	 *
	 * @param id
	 *            the Long id array
	 * @return the string
	 */
	public String createIdSubQuery(Long[] id) {
		StringBuilder sb = new StringBuilder();
		int length = id.length;
		for (int i = 0; i < length - 1; i++) {
			sb.append(id[i]).append(",");
		}
		sb.append(id[length-1]);
		return " in(" + sb.toString() + ")";
	}

	/**
	 * Creates the sub query.
	 *
	 * @param searchCriteria
	 *            the search criteria object
	 * @return the string SQL sub query
	 */
	public String createSearchSubQuery(SearchCriteria searchCriteria) {
		String authorSubQuery = createAuthorSubQuery(searchCriteria.getAuthor());
		String tagSubQuery = createTagSubQuery(searchCriteria.getTagList());
		String subQuery = null;
		if (authorSubQuery != null) {
			subQuery = authorSubQuery;
		}
		if (tagSubQuery != null) {
			subQuery = tagSubQuery;
		}
		if (authorSubQuery != null && tagSubQuery != null) {
			subQuery = authorSubQuery + " and" + tagSubQuery;
		}
		return subQuery;
	}

	/**
	 * Creates the author sub query.
	 *
	 * @param author
	 *            the author object
	 * @return the string SQL author sub query
	 */
	private String createAuthorSubQuery(Author author) {
		String authorSubQuery = null;
		if (author != null) {
			authorSubQuery = " NEWS_AUTHOR.AUTHOR_ID=" + author.getAuthorId();
		}
		return authorSubQuery;
	}

	/**
	 * Creates the tag sub query.
	 *
	 * @param tagList
	 *            the tag list object
	 * @return the string SQL tag sub query
	 */
	private String createTagSubQuery(List<Tag> tagList) {
		String tagSubQuery = null;
		StringBuilder sb = new StringBuilder();
		if (tagList != null) {
			int listSize = tagList.size();
			for (int i = 0; i < listSize - 1; i++) {
				sb.append(tagList.get(i).getTagId()).append(",");
			}
			sb.append(tagList.get(listSize-1).getTagId());
			tagSubQuery = " NEWS_tag.tag_ID in (" + sb.toString() + ")";
		}
		return tagSubQuery;
	}
}
