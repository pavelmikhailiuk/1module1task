package by.epam.newsdao.dao.impl;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Repository;

import by.epam.newsdao.constant.TablesFieldsConstants;
import by.epam.newsdao.dao.TagDAO;
import by.epam.newsdao.dao.util.DaoUtils;
import by.epam.newsdao.entity.Tag;
import by.epam.newsdao.exception.DAOException;

/**
 * The Class TagDAOImpl. Implements interface TagDAO
 */
@Repository
public class TagDAOImpl implements TagDAO {

	/** The Constant SQL_FIND_ALL. */
	private static final String SQL_FIND_ALL = "select tag_id, tag_name from Tag";

	/** The Constant SQL_DELETE. */
	private static final String SQL_DELETE = "delete from Tag where tag_id=?";

	/** The Constant SQL_UPDATE. */
	private static final String SQL_UPDATE = "update Tag set tag_name=? where tag_id = ?";

	/** The Constant SQL_FIND. */
	private static final String SQL_FIND = "select tag_id, tag_name from Tag where tag_id = ?";

	/** The Constant SQL_SAVE. */
	private static final String SQL_SAVE = "insert into Tag (tag_name) values (?)";

	/** The data source. */
	@Autowired
	private DataSource dataSource;

	/** The dao utils. */
	@Autowired
	private DaoUtils daoUtils;
	/*
	 * (non-Javadoc)
	 * 
	 * @see by.epam.newsmanagement.dao.CRUDDAO#save(java.lang.Object)
	 */
	@Override
	public Long save(Tag tag) throws DAOException {
		Long id = null;
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_SAVE,
				new String[] { TablesFieldsConstants.TAG_ID })) {
			preparedStatement.setString(1, tag.getTagName());
			preparedStatement.executeUpdate();
			ResultSet tableKeys = preparedStatement.getGeneratedKeys();
			tableKeys.next();
			id = tableKeys.getLong(1);
		} catch (SQLException e) {
			throw new DAOException("Error save tag", e);
		} finally {
			daoUtils.releaseConnection(connection, dataSource);
		}
		return id;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see by.epam.newsmanagement.dao.CRUDDAO#find(java.lang.Long)
	 */
	@Override
	public Tag find(Long id) throws DAOException {
		Tag tag = null;
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_FIND)) {
			preparedStatement.setLong(1, id);
			ResultSet resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				tag = initTag(resultSet);
			}
		} catch (SQLException e) {
			throw new DAOException("Error find tag by id", e);
		} finally {
			daoUtils.releaseConnection(connection, dataSource);
		}
		return tag;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see by.epam.newsmanagement.dao.CRUDDAO#update(java.lang.Object)
	 */
	@Override
	public void update(Tag tag) throws DAOException {
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_UPDATE)) {
			preparedStatement.setString(1, tag.getTagName());
			preparedStatement.setLong(2, tag.getTagId());
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException("Error update tag", e);
		} finally {
			daoUtils.releaseConnection(connection, dataSource);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see by.epam.newsmanagement.dao.CRUDDAO#delete(java.lang.Long)
	 */
	@Override
	public void delete(Long id) throws DAOException {
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_DELETE)) {
			preparedStatement.setLong(1, id);
			preparedStatement.executeQuery();
		} catch (SQLException e) {
			throw new DAOException("Error delete tag by id", e);
		} finally {
			daoUtils.releaseConnection(connection, dataSource);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see by.epam.newsmanagement.dao.CRUDDAO#findAll()
	 */
	@Override
	public List<Tag> findAll() throws DAOException {
		List<Tag> tagList = new ArrayList<>();
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_FIND_ALL)) {
			ResultSet resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				tagList.add(initTag(resultSet));
			}
		} catch (SQLException e) {
			throw new DAOException("Error find all tags", e);
		} finally {
			daoUtils.releaseConnection(connection, dataSource);
		}
		return tagList;
	}

	/**
	 * Inits the tag.
	 *
	 * @param rs
	 *            the resultset
	 * @return the tag
	 * @throws DAOException
	 *             the DAO exception if operation failed
	 */
	private Tag initTag(ResultSet rs) throws DAOException {
		Tag tag = new Tag();
		try {
			tag.setTagId(rs.getLong(TablesFieldsConstants.TAG_ID));
			tag.setTagName(rs.getString(TablesFieldsConstants.TAG_NAME));
		} catch (SQLException e) {
			throw new DAOException("Error tag initialization", e);
		}
		return tag;
	}
}
