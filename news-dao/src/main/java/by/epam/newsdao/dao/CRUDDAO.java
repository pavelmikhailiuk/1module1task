package by.epam.newsdao.dao;

import java.util.List;

import by.epam.newsdao.exception.DAOException;

/**
 * The Interface CRUDDAO for basic CRUD operations
 *
 * @param <T> the generic type
 */
public interface CRUDDAO<T> {

	/**
	 * Save.
	 *
	 * @param entity the entity of generic type
	 * @return the long
	 * @throws DAOException the DAO exception if operation failed
	 */
	Long save(T entity) throws DAOException;

	/**
	 * Find.
	 *
	 * @param id the id
	 * @return the generic type
	 * @throws DAOException the DAO exception if operation failed
	 */
	T find(Long id) throws DAOException;

	/**
	 * Update.
	 *
	 * @param entity the entity of generic type
	 * @throws DAOException the DAO exception if operation failed
	 */
	void update(T entity) throws DAOException;

	/**
	 * Delete.
	 *
	 * @param id the id
	 * @throws DAOException the DAO exception if operation failed
	 */
	void delete(Long id) throws DAOException;

	/**
	 * Find all.
	 *
	 * @return the list of the generic type
	 * @throws DAOException the DAO exception if operation failed
	 */
	List<T> findAll() throws DAOException;
}
