package by.epam.newsdao.exception;

/**
 * The Class DAOException.
 */
public class DAOException extends Exception {
	/** The Constant serialVersionUID. */
	public static final long serialVersionUID = 1L;
	/**
	 * Instantiates a new DAO exception.
	 */
	public DAOException() {
	}

		/**
	 * Instantiates a new DAO exception.
	 *
	 * @param message the message
	 * @param cause the cause
	 */
	public DAOException(String message, Throwable cause) {
		super(message, cause);
	}
	}
