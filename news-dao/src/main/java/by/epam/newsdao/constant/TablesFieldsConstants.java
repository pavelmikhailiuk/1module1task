package by.epam.newsdao.constant;


/**
 * The Class TablesFieldsConstants. Contains constants named as database tables fields.
 */
public class TablesFieldsConstants {

	/** The Constant EXPIRED. */
	public static final String EXPIRED = "expired";
	
	/** The Constant AUTHOR_NAME. */
	public static final String AUTHOR_NAME = "author_name";
	
	/** The Constant AUTHOR_ID. */
	public static final String AUTHOR_ID = "author_id";
	
	/** The Constant CREATION_DATE. */
	public static final String CREATION_DATE = "creation_date";
	
	/** The Constant COMMENT_TEXT. */
	public static final String COMMENT_TEXT = "comment_text";
	
	/** The Constant NEWS_ID. */
	public static final String NEWS_ID = "news_id";
	
	/** The Constant COMMENT_ID. */
	public static final String COMMENT_ID = "comment_id";
	
	/** The Constant ROWCOUNT. */
	public static final String ROWCOUNT = "rowcount";
	
	/** The Constant MODIFICATION_DATE. */
	public static final String MODIFICATION_DATE = "modification_date";
	
	/** The Constant FULL_TEXT. */
	public static final String FULL_TEXT = "full_text";
	
	/** The Constant SHORT_TEXT. */
	public static final String SHORT_TEXT = "short_text";
	
	/** The Constant TITLE. */
	public static final String TITLE = "title";
	
	/** The Constant TAG_ID. */
	public static final String TAG_ID = "tag_id";
	
	/** The Constant TAG_NAME. */
	public static final String TAG_NAME = "tag_name";
}
