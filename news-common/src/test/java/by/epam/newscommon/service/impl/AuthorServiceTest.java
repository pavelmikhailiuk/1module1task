package by.epam.newscommon.service.impl;

import static com.googlecode.catchexception.CatchException.verifyException;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.mockito.runners.MockitoJUnitRunner;

import by.epam.newscommon.exception.ServiceException;
import by.epam.newscommon.service.impl.AuthorServiceImpl;
import by.epam.newsdao.dao.AuthorDAO;
import by.epam.newsdao.entity.Author;
import by.epam.newsdao.exception.DAOException;

/**
 * The Class AuthorServiceTest.
 */
@RunWith(MockitoJUnitRunner.class)
public class AuthorServiceTest {

	/** The author DAO. */
	@Mock
	private AuthorDAO authorDAO;

	/** The author service. */
	@InjectMocks
	private AuthorServiceImpl authorService;

	/**
	 * Mock objects initialization.
	 */
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	/**
	 * Test save author.
	 *
	 * @throws DAOException
	 *             the DAO exception if database operation failed
	 * @throws ServiceException
	 *             the service exception if service operation failed
	 */
	@Test
	public void testSaveAuthor() throws DAOException, ServiceException {
		Author author = initAuthor();
		long expectedId = 111L;
		when(authorService.save(author)).thenReturn(expectedId);
		long actualId = authorService.save(author);
		verify(authorDAO).save(author);
		assertEquals(expectedId, actualId);
	}

	/**
	 * Test find author.
	 *
	 * @throws DAOException
	 *             the DAO exception if database operation failed
	 * @throws ServiceException
	 *             the service exception if service operation failed
	 */
	@Test
	public void testFindAuthor() throws DAOException, ServiceException {
		Author expectedAuthor = initAuthor();
		long authorId = authorService.save(expectedAuthor);
		when(authorService.find(authorId)).thenReturn(expectedAuthor);
		Author actualAuthor = authorService.find(authorId);
		verify(authorDAO).find(authorId);
		assertEquals(expectedAuthor, actualAuthor);
	}

	/**
	 * Test update author.
	 *
	 * @throws DAOException
	 *             the DAO exception if database operation failed
	 * @throws ServiceException
	 *             the service exception if service operation failed
	 */
	@Test
	public void testUpdateAuthor() throws DAOException, ServiceException {
		Author expectedAuthor = initAuthor();
		when(authorService.update(expectedAuthor)).thenReturn(expectedAuthor);
		Author actualAuthor = authorService.update(expectedAuthor);
		assertEquals(expectedAuthor, actualAuthor);
	}

	/**
	 * Test find all author.
	 *
	 * @throws DAOException
	 *             the DAO exception if database operation failed
	 * @throws ServiceException
	 *             the service exception if service operation failed
	 */
	@Test
	public void testFindAllAuthor() throws DAOException, ServiceException {
		List<Author> authorList = new ArrayList<Author>();
		authorList.add(initAuthor());
		authorList.add(initAuthor());
		when(authorService.findAll()).thenReturn(authorList);
		List<Author> actualAuthorList = authorService.findAll();
		verify(authorDAO).findAll();
		assertEquals(actualAuthorList.size(), 2);
	}

	/**
	 * Test delete author.
	 *
	 * @throws DAOException the DAO exception
	 * @throws ServiceException the service exception
	 */
	@Test(expected = UnsupportedOperationException.class)
	public void testDeleteAuthor() throws DAOException, ServiceException {
		authorService.delete(1L);
	}
	/**
	 * Test save exception.
	 * 
	 * 
	 * @throws ServiceException
	 * @throws DAOException
	 */
	@Test
	public void testSaveException() throws ServiceException, DAOException {
		Author author = null;
		when(authorDAO.save(author)).thenThrow(new DAOException());
		verifyException(authorService, ServiceException.class).save(author);
	}

	/**
	 * Test find all exception.
	 * 
	 * 
	 * @throws ServiceException
	 * @throws DAOException
	 */
	@Test
	public void testFindAllException() throws ServiceException, DAOException {
		when(authorDAO.findAll()).thenThrow(new DAOException());
		verifyException(authorService, ServiceException.class).findAll();
	}

	/**
	 * Test find exception.
	 * 
	 * 
	 * @throws ServiceException
	 * @throws DAOException
	 */
	@Test
	public void testFindException() throws ServiceException,
			DAOException {
		when(authorDAO.find(1L)).thenThrow(new DAOException());
		verifyException(authorService, ServiceException.class)
				.find(1L);
	}

	/**
	 * Inits the author.
	 *
	 * @return the author
	 */
	private Author initAuthor() {
		return new Author(111L, "Jimmy", new Date());
	}
}
