package by.epam.newscommon.service.impl;

import static com.googlecode.catchexception.CatchException.verifyException;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.mockito.runners.MockitoJUnitRunner;

import by.epam.newscommon.exception.ServiceException;
import by.epam.newscommon.service.impl.CommentServiceImpl;
import by.epam.newsdao.dao.CommentDAO;
import by.epam.newsdao.entity.Comment;
import by.epam.newsdao.exception.DAOException;

/**
 * The Class CommentServiceTest.
 */
@RunWith(MockitoJUnitRunner.class)
public class CommentServiceTest {

	/** The comment DAO. */
	@Mock
	private CommentDAO commentDAO;

	/** The comment service. */
	@InjectMocks
	private CommentServiceImpl commentService;

	/**
	 * Mock objects initialization.
	 */
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	/**
	 * Test save comment.
	 *
	 * @throws DAOException
	 *             the DAO exception if database operation failed
	 * @throws ServiceException
	 *             the service exception if service operation failed
	 */
	@Test
	public void testSaveComment() throws DAOException, ServiceException {
		Comment comment = initComment();
		long expectedId = 111L;
		when(commentService.save(comment)).thenReturn(expectedId);
		long actualId = commentService.save(comment);
		verify(commentDAO).save(comment);
		assertEquals(expectedId, actualId);
	}

	/**
	 * Test find comment.
	 *
	 * @throws DAOException
	 *             the DAO exception if database operation failed
	 * @throws ServiceException
	 *             the service exception if service operation failed
	 */
	@Test
	public void testFindComment() throws DAOException, ServiceException {
		Comment expectedComment = initComment();
		long commentId = commentService.save(expectedComment);
		when(commentService.find(commentId)).thenReturn(expectedComment);
		Comment actualComment = commentService.find(commentId);
		verify(commentDAO).find(commentId);
		assertEquals(expectedComment, actualComment);
	}

	/**
	 * Test update comment.
	 *
	 * @throws DAOException
	 *             the DAO exception if database operation failed
	 * @throws ServiceException
	 *             the service exception if service operation failed
	 */
	@Test
	public void testUpdateComment() throws DAOException, ServiceException {
		Comment expectedComment = initComment();
		when(commentService.update(expectedComment))
				.thenReturn(expectedComment);
		Comment actualComment = commentService.update(expectedComment);
		assertEquals(expectedComment, actualComment);
	}

	/**
	 * Test delete comment by news id.
	 *
	 * @throws DAOException
	 *             the DAO exception if database operation failed
	 * @throws ServiceException
	 *             the service exception if service operation failed
	 */
	@Test
	public void testDeleteCommentByNewsId() throws DAOException,
			ServiceException {
		Long[] newsId = new Long[] { 1l };
		commentService.deleteByNewsId(newsId);
		verify(commentDAO).deleteByNewsId(newsId);
	}

	/**
	 * Test delete comment.
	 *
	 * @throws DAOException
	 *             the DAO exception if database operation failed
	 * @throws ServiceException
	 *             the service exception if service operation failed
	 */
	@Test
	public void testDeleteComment() throws DAOException, ServiceException {
		Comment comment = initComment();
		commentService.delete(comment.getCommentId());
		verify(commentDAO).delete(comment.getCommentId());
	}

	/**
	 * Test find all comment.
	 *
	 * @throws DAOException
	 *             the DAO exception if database operation failed
	 * @throws ServiceException
	 *             the service exception if service operation failed
	 */
	@Test
	public void testFindAllComment() throws DAOException, ServiceException {
		List<Comment> commentList = new ArrayList<Comment>();
		commentList.add(initComment());
		commentList.add(initComment());
		when(commentService.findAll()).thenReturn(commentList);
		List<Comment> actualCommentList = commentService.findAll();
		verify(commentDAO).findAll();
		assertEquals(actualCommentList.size(), 2);
	}

	/**
	 * Test find by news id.
	 *
	 * @throws DAOException
	 *             the DAO exception if database operation failed
	 * @throws ServiceException
	 *             the service exception if service operation failed
	 */
	@Test
	public void testFindByNewsId() throws DAOException, ServiceException {
		List<Comment> expectedCommentList = new ArrayList<Comment>();
		expectedCommentList.add(initComment());
		expectedCommentList.add(initComment());
		when(commentService.findByNewsId(1L)).thenReturn(expectedCommentList);
		List<Comment> actualCommentList = commentService.findByNewsId(1L);
		verify(commentDAO).findAll();
		assertEquals(actualCommentList, expectedCommentList);
	}

	/**
	 * Test save exception.
	 * 
	 * 
	 * @throws ServiceException
	 * @throws DAOException
	 */
	@Test
	public void testSaveException() throws ServiceException, DAOException {
		Comment comment = null;
		when(commentDAO.save(comment)).thenThrow(new DAOException());
		verifyException(commentService, ServiceException.class).save(comment);
	}

	/**
	 * Test find all exception.
	 * 
	 * 
	 * @throws ServiceException
	 * @throws DAOException
	 */
	@Test
	public void testFindAllException() throws ServiceException, DAOException {
		when(commentDAO.findAll()).thenThrow(new DAOException());
		verifyException(commentService, ServiceException.class).findAll();
	}

	/**
	 * Test find by news id exception.
	 * 
	 * 
	 * @throws ServiceException
	 * @throws DAOException
	 */
	@Test
	public void testFindByNewsIdException() throws ServiceException,
			DAOException {
		when(commentDAO.findAll()).thenThrow(new DAOException());
		verifyException(commentService, ServiceException.class)
				.findByNewsId(1L);
	}

	/**
	 * Inits the comment.
	 *
	 * @return the comment
	 */
	private Comment initComment() {
		return new Comment(111L, 1l, "comment", new Date());
	}
}
