package by.epam.newscommon.service.impl;

import static com.googlecode.catchexception.CatchException.verifyException;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.mockito.runners.MockitoJUnitRunner;

import by.epam.newscommon.exception.ServiceException;
import by.epam.newscommon.service.impl.NewsServiceImpl;
import by.epam.newsdao.dao.NewsDAO;
import by.epam.newsdao.entity.*;
import by.epam.newsdao.exception.DAOException;

/**
 * The Class NewsServiceTest.
 */
@RunWith(MockitoJUnitRunner.class)
public class NewsServiceTest {

	/** The news DAO. */
	@Mock
	private NewsDAO newsDAO;

	/** The news service. */
	@InjectMocks
	private NewsServiceImpl newsService;

	/**
	 * Mock objects initialization.
	 */
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	/**
	 * Test save news.
	 *
	 * @throws DAOException
	 *             the DAO exception if database operation failed
	 * @throws ServiceException
	 *             the service exception if service operation failed
	 */
	@Test
	public void testSaveNews() throws DAOException, ServiceException {
		News news = initNews();
		long expectedId = 111L;
		when(newsService.save(news)).thenReturn(expectedId);
		long actualId = newsService.save(news);
		verify(newsDAO).save(news);
		assertEquals(expectedId, actualId);
	}

	/**
	 * Test find news.
	 *
	 * @throws DAOException
	 *             the DAO exception if database operation failed
	 * @throws ServiceException
	 *             the service exception if service operation failed
	 */
	@Test
	public void testFindNews() throws DAOException, ServiceException {
		News expectedNews = initNews();
		long newsId = 1l;
		when(newsService.find(newsId)).thenReturn(expectedNews);
		News actualNews = newsService.find(newsId);
		verify(newsDAO).find(newsId);
		assertEquals(expectedNews, actualNews);
	}

	/**
	 * Test update news.
	 *
	 * @throws DAOException
	 *             the DAO exception if database operation failed
	 * @throws ServiceException
	 *             the service exception if service operation failed
	 */
	@Test
	public void testUpdateNews() throws DAOException, ServiceException {
		News expectedNews = initNews();
		when(newsService.update(expectedNews)).thenReturn(expectedNews);
		News actualNews = newsService.update(expectedNews);
		assertEquals(expectedNews, actualNews);
	}

	/**
	 * Test find all news.
	 *
	 * @throws DAOException
	 *             the DAO exception if database operation failed
	 * @throws ServiceException
	 *             the service exception if service operation failed
	 */
	@Test
	public void testFindAllNews() throws DAOException, ServiceException {
		List<News> newsList = new ArrayList<News>();
		newsList.add(initNews());
		newsList.add(initNews());
		when(newsService.findAll()).thenReturn(newsList);
		List<News> actualNewsList = newsService.findAll();
		verify(newsDAO).findAll();
		assertEquals(actualNewsList.size(), 2);
	}

	/**
	 * Test find all news with pagination.
	 *
	 * @throws DAOException
	 *             the DAO exception if database operation failed
	 * @throws ServiceException
	 *             the service exception if service operation failed
	 */
	@Test
	public void testFindAllNewsPagination() throws DAOException,
			ServiceException {
		List<News> newsList = new ArrayList<News>();
		newsList.add(initNews());
		newsList.add(initNews());
		when(newsService.findAll(0, 3)).thenReturn(newsList);
		List<News> actualNewsList = newsService.findAll(0, 3);
		verify(newsDAO).findAll(0, 3);
		assertEquals(actualNewsList.size(), 2);
	}

	/**
	 * Test find by search criteria.
	 *
	 * @throws DAOException
	 *             the DAO exception if database operation failed
	 * @throws ServiceException
	 *             the service exception if service operation failed
	 */
	@Test
	public void testFindBySearchCriteria() throws DAOException,
			ServiceException {
		List<News> newsList = new ArrayList<News>();
		newsList.add(initNews());
		newsList.add(initNews());
		SearchCriteria sc = new SearchCriteria();
		Author author = new Author(111l, "Jimmy", new Date());
		List<Tag> tags = new ArrayList<Tag>();
		tags.add(new Tag(1l, "misc"));
		sc.setAuthor(author);
		sc.setTagList(tags);
		when(newsService.find(sc, 0, 2)).thenReturn(newsList);
		List<News> actualNewsList = newsService.find(sc, 0, 2);
		verify(newsDAO).findBySearchCriteria(sc, 0, 2);
		assertEquals(actualNewsList.size(), 2);
	}

	/**
	 * Test count news.
	 *
	 * @throws DAOException
	 *             the DAO exception if database operation failed
	 * @throws ServiceException
	 *             the service exception if service operation failed
	 */
	@Test
	public void testCountNews() throws DAOException, ServiceException {
		long expectedNewsCount = 10l;
		when(newsService.countNews()).thenReturn(expectedNewsCount);
		long actualNewsCount = newsService.countNews();
		verify(newsDAO).newsCount();
		assertEquals(expectedNewsCount, actualNewsCount);
	}

	/**
	 * Test count news with search criteria.
	 *
	 * @throws DAOException
	 *             the DAO exception if database operation failed
	 * @throws ServiceException
	 *             the service exception if service operation failed
	 */
	@Test
	public void testCountNewsSearch() throws DAOException, ServiceException {
		long expectedNewsCount = 10l;
		SearchCriteria sc = null;
		when(newsService.countNews(sc)).thenReturn(expectedNewsCount);
		long actualNewsCount = newsService.countNews(sc);
		verify(newsDAO).newsCount(sc);
		assertEquals(expectedNewsCount, actualNewsCount);
	}

	/**
	 * Test delete news.
	 *
	 * @throws DAOException
	 *             the DAO exception if database operation failed
	 * @throws ServiceException
	 *             the service exception if service operation failed
	 */
	@Test
	public void testDeleteNews() throws DAOException, ServiceException {
		News news = initNews();
		newsService.delete(news.getNewsId());
		verify(newsDAO).delete(news.getNewsId());
	}

	/**
	 * Test delete by array of the news id.
	 *
	 * @throws DAOException
	 *             the DAO exception if database operation failed
	 * @throws ServiceException
	 *             the service exception if service operation failed
	 */
	@Test
	public void testDeleteNewsByArrayOfId() throws DAOException,
			ServiceException {
		Long[] newsId = new Long[] { 1l };
		newsService.delete(newsId);
		verify(newsDAO).delete(newsId);
	}

	/**
	 * Test save news author.
	 *
	 * @throws DAOException
	 *             the DAO exception if database operation failed
	 * @throws ServiceException
	 *             the service exception if service operation failed
	 */
	@Test
	public void testSaveNewsAuthor() throws DAOException, ServiceException {
		long newsId = 1l;
		long authorId = 1l;
		newsService.saveNewsAuthor(newsId, authorId);
		verify(newsDAO).saveNewsAuthor(newsId, authorId);
	}

	/**
	 * Test update news author.
	 *
	 * @throws DAOException
	 *             the DAO exception if database operation failed
	 * @throws ServiceException
	 *             the service exception if service operation failed
	 */
	@Test
	public void testUpdateNewsAuthor() throws DAOException, ServiceException {
		long newsId = 1l;
		long authorId = 1l;
		newsService.updateNewsAuthor(newsId, authorId);
		verify(newsDAO).updateNewsAuthor(newsId, authorId);
	}

	/**
	 * Test delete news author.
	 *
	 * @throws DAOException
	 *             the DAO exception if database operation failed
	 * @throws ServiceException
	 *             the service exception if service operation failed
	 */
	@Test
	public void testDeleteNewsAuthor() throws DAOException, ServiceException {
		Long[] newsId = new Long[] { 1L };
		newsService.deleteNewsAuthor(newsId);
		verify(newsDAO).deleteNewsAuthor(newsId);
	}

	/**
	 * Test save news tag.
	 *
	 * @throws DAOException
	 *             the DAO exception if database operation failed
	 * @throws ServiceException
	 *             the service exception if service operation failed
	 */
	@Test
	public void testSaveNewsTag() throws DAOException, ServiceException {
		long newsId = 1l;
		long tagId = 1l;
		newsService.saveNewsTag(newsId, tagId);
		verify(newsDAO).saveNewsTag(newsId, tagId);
	}

	/**
	 * Test update news tag.
	 *
	 * @throws DAOException
	 *             the DAO exception if database operation failed
	 * @throws ServiceException
	 *             the service exception if service operation failed
	 */
	@Test
	public void testUpdateNewsTag() throws DAOException, ServiceException {
		long newsId = 1l;
		long tagId = 1l;
		newsService.updateNewsTag(newsId, tagId);
		verify(newsDAO).updateNewsTag(newsId, tagId);
	}

	/**
	 * Test delete news tag by news id.
	 *
	 * @throws DAOException
	 *             the DAO exception if database operation failed
	 * @throws ServiceException
	 *             the service exception if service operation failed
	 */
	@Test
	public void testDeleteNewsTagByNewsId() throws DAOException,
			ServiceException {
		Long[] newsId = new Long[] { 1L };
		newsService.deleteNewsTagByNewsId(newsId);
		verify(newsDAO).deleteNewsTagByNewsId(newsId);
	}

	/**
	 * Test delete news tag by tag id.
	 *
	 * @throws DAOException
	 *             the DAO exception if database operation failed
	 * @throws ServiceException
	 *             the service exception if service operation failed
	 */
	@Test
	public void testDeleteNewsTagByTagId() throws DAOException,
			ServiceException {
		long tagId = 1l;
		newsService.deleteNewsTagByTagId(tagId);
		verify(newsDAO).deleteNewsTagByTagId(tagId);
	}

	/**
	 * Test find author id by news id.
	 *
	 * @throws DAOException
	 *             the DAO exception if database operation failed
	 * @throws ServiceException
	 *             the service exception if service operation failed
	 */
	@Test
	public void testFindAuthorIdByNewsId() throws DAOException,
			ServiceException {
		long newsId = 1l;
		long expectedAuthorId = 10l;
		when(newsService.findAuthorIdByNewsId(newsId)).thenReturn(
				expectedAuthorId);
		long actualAuthorId = newsService.findAuthorIdByNewsId(newsId);
		verify(newsDAO).findAuthorIdByNewsId(newsId);
		assertEquals(expectedAuthorId, actualAuthorId);
	}

	/**
	 * Test find tag id by news id.
	 *
	 * @throws DAOException
	 *             the DAO exception if database operation failed
	 * @throws ServiceException
	 *             the service exception if service operation failed
	 */
	@Test
	public void testFindTagIdByNewsId() throws DAOException, ServiceException {
		long newsId = 1l;
		List<Long> expectedTagIdList = new ArrayList<Long>();
		expectedTagIdList.add(10L);
		expectedTagIdList.add(5L);
		when(newsService.findTagIdByNewsId(newsId)).thenReturn(
				expectedTagIdList);
		List<Long> actualTagIdList = newsService.findTagIdByNewsId(newsId);
		verify(newsDAO).findTagIdByNewsId(newsId);
		assertEquals(expectedTagIdList, actualTagIdList);
	}

	/**
	 * Test save exception.
	 * 
	 * 
	 * @throws ServiceException
	 * @throws DAOException
	 */
	@Test
	public void testSaveException() throws ServiceException, DAOException {
		News news = null;
		when(newsDAO.save(news)).thenThrow(new DAOException());
		verifyException(newsService, ServiceException.class).save(news);
	}

	/**
	 * Test find all exception.
	 * 
	 * 
	 * @throws ServiceException
	 * @throws DAOException
	 */
	@Test
	public void testFindAllException() throws ServiceException, DAOException {
		when(newsDAO.findAll()).thenThrow(new DAOException());
		verifyException(newsService, ServiceException.class).findAll();
	}

	/**
	 * Test find exception.
	 * 
	 * 
	 * @throws ServiceException
	 * @throws DAOException
	 */
	@Test
	public void testFindException() throws ServiceException, DAOException {
		when(newsDAO.find(1L)).thenThrow(new DAOException());
		verifyException(newsService, ServiceException.class).find(1L);
	}

	/**
	 * Inits the news.
	 *
	 * @return the news
	 */
	private News initNews() {
		return new News(1l, "title", "short text", "full text", new Date(),
				new Date());
	}
}
