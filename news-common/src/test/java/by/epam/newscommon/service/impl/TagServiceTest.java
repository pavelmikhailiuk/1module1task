package by.epam.newscommon.service.impl;

import static com.googlecode.catchexception.CatchException.verifyException;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.mockito.runners.MockitoJUnitRunner;

import by.epam.newscommon.exception.ServiceException;
import by.epam.newscommon.service.impl.TagServiceImpl;
import by.epam.newsdao.dao.TagDAO;
import by.epam.newsdao.entity.Tag;
import by.epam.newsdao.exception.DAOException;

/**
 * The Class TagServiceTest.
 */
@RunWith(MockitoJUnitRunner.class)
public class TagServiceTest {

	/** The tag DAO. */
	@Mock
	private TagDAO tagDAO;

	/** The tag service. */
	@InjectMocks
	private TagServiceImpl tagService;

	/**
	 * Mock objects initialization.
	 */
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	/**
	 * Test save tag.
	 *
	 * @throws DAOException
	 *             the DAO exception if database operation failed
	 * @throws ServiceException
	 *             the service exception if service operation failed
	 */
	@Test
	public void testSaveTag() throws DAOException, ServiceException {
		Tag tag = initTag();
		long expectedId = 111L;
		when(tagService.save(tag)).thenReturn(expectedId);
		long actualId = tagService.save(tag);
		verify(tagDAO).save(tag);
		assertEquals(expectedId, actualId);
	}

	/**
	 * Test find tag.
	 *
	 * @throws DAOException
	 *             the DAO exception if database operation failed
	 * @throws ServiceException
	 *             the service exception if service operation failed
	 */
	@Test
	public void testFindTag() throws DAOException, ServiceException {
		Tag expectedTag = initTag();
		long tagId = tagService.save(expectedTag);
		when(tagService.find(tagId)).thenReturn(expectedTag);
		Tag actualTag = tagService.find(tagId);
		verify(tagDAO).find(tagId);
		assertEquals(expectedTag, actualTag);
	}

	/**
	 * Test update tag.
	 * 
	 * @throws DAOException
	 *             the DAO exception if database operation failed
	 * @throws ServiceException
	 *             the service exception if service operation failed
	 */
	@Test
	public void testUpdateTag() throws DAOException, ServiceException {
		Tag expectedTag = initTag();
		when(tagService.update(expectedTag)).thenReturn(expectedTag);
		Tag actualTag = tagService.update(expectedTag);
		assertEquals(expectedTag, actualTag);
	}

	/**
	 * Test find all tag.
	 *
	 * @throws DAOException
	 *             the DAO exception if database operation failed
	 * @throws ServiceException
	 *             the service exception if service operation failed
	 */
	@Test
	public void testFindAllTag() throws DAOException, ServiceException {
		List<Tag> tagList = new ArrayList<Tag>();
		tagList.add(initTag());
		tagList.add(initTag());
		when(tagService.findAll()).thenReturn(tagList);
		List<Tag> actualTagList = tagService.findAll();
		verify(tagDAO).findAll();
		assertEquals(actualTagList.size(), 2);
	}
	/**
	 * Test save exception.
	 * 
	 * 
	 * @throws ServiceException
	 * @throws DAOException
	 */
	@Test
	public void testSaveException() throws ServiceException, DAOException {
		Tag tag = null;
		when(tagDAO.save(tag)).thenThrow(new DAOException());
		verifyException(tagService, ServiceException.class).save(tag);
	}

	/**
	 * Test find all exception.
	 * 
	 * 
	 * @throws ServiceException
	 * @throws DAOException
	 */
	@Test
	public void testFindAllException() throws ServiceException, DAOException {
		when(tagDAO.findAll()).thenThrow(new DAOException());
		verifyException(tagService, ServiceException.class).findAll();
	}

	/**
	 * Test find exception.
	 * 
	 * 
	 * @throws ServiceException
	 * @throws DAOException
	 */
	@Test
	public void testFindException() throws ServiceException,
			DAOException {
		when(tagDAO.find(1L)).thenThrow(new DAOException());
		verifyException(tagService, ServiceException.class)
				.find(1L);
	}


	/**
	 * Inits the tag.
	 *
	 * @return the tag
	 */
	private Tag initTag() {
		return new Tag(111L, "Tag");
	}
}
