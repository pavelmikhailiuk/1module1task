package by.epam.newscommon.service.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.mockito.runners.MockitoJUnitRunner;

import by.epam.newscommon.exception.ServiceException;
import by.epam.newscommon.service.*;
import by.epam.newscommon.service.impl.NewsManagementServiceImpl;
import by.epam.newsdao.entity.*;
import by.epam.newsdao.exception.DAOException;

/**
 * The Class NewsManagementServiceTest.
 */
@RunWith(MockitoJUnitRunner.class)
public class NewsManagementServiceTest {

	/** The news service. */
	@Mock
	private NewsService newsService;

	/** The comment service. */
	@Mock
	private CommentService commentService;

	/** The tag service. */
	@Mock
	private TagService tagService;

	/** The author service. */
	@Mock
	private AuthorService authorService;

	/** The news management service. */
	@InjectMocks
	private NewsManagementServiceImpl newsManagementService;

	/**
	 * Mock objects initialization.
	 */
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	/**
	 * Test add news.
	 *
	 * @throws DAOException
	 *             the DAO exception if database operation failed
	 * @throws ServiceException
	 *             the service exception if service operation failed
	 */
	@Test
	public void testAddNews() throws DAOException, ServiceException {
		News news = initNews();
		Author author = initAuthor();
		List<Tag> tagList = new ArrayList<>();
		Tag tag = initTag();
		tagList.add(tag);
		List<Comment> commentList = new ArrayList<>();
		NewsDTO newsDTO = new NewsDTO(news, author, tagList, commentList);
		newsManagementService.addNews(newsDTO);
		long newsId = news.getNewsId();
		verify(newsService).save(news);
		verify(newsService).saveNewsTag(newsId, tag.getTagId());
		verify(newsService).saveNewsAuthor(newsId, author.getAuthorId());
	}

	/**
	 * Test edit news.
	 *
	 * @throws DAOException
	 *             the DAO exception if database operation failed
	 * @throws ServiceException
	 *             the service exception if service operation failed
	 */
	@Test
	public void testEditNews() throws DAOException, ServiceException {
		News news = initNews();
		Author author = initAuthor();
		List<Tag> tagList = new ArrayList<>();
		Tag tag = initTag();
		tagList.add(tag);
		NewsDTO newsDTO = new NewsDTO(news, author, tagList, null);
		newsManagementService.editNews(newsDTO);
		long newsId = news.getNewsId();
		Long[] newsID = new Long[] { newsId };
		verify(newsService).update(news);
		verify(newsService).updateNewsAuthor(newsId, author.getAuthorId());
		verify(newsService).deleteNewsTagByNewsId(newsID);
		verify(newsService).saveNewsTag(newsId, tag.getTagId());
	}

	/**
	 * Test find all news.
	 *
	 * @throws DAOException
	 *             the DAO exception if database operation failed
	 * @throws ServiceException
	 *             the service exception if service operation failed
	 */

	@Test
	public void testFindAllNews() throws DAOException, ServiceException {
		List<NewsDTO> expectedNewsDTOList = new ArrayList<NewsDTO>();
		when(newsManagementService.findAllNews(0, 3)).thenReturn(
				expectedNewsDTOList);
		List<NewsDTO> actualNewsDTOList = newsManagementService.findAllNews(0,
				3);
		verify(newsService).findAll(0, 3);
		assertEquals(actualNewsDTOList, expectedNewsDTOList);
	}

	/**
	 * Test find news.
	 *
	 * @throws DAOException
	 *             the DAO exception if database operation failed
	 * @throws ServiceException
	 *             the service exception if service operation failed
	 */
	@Test
	public void testFindNews() throws DAOException, ServiceException {
		Long newsId = 1l;
		newsManagementService.findNews(newsId);
		verify(newsService).find(newsId);
	}

	/**
	 * Test find by search criteria.
	 *
	 * @throws DAOException
	 *             the DAO exception if database operation failed
	 * @throws ServiceException
	 *             the service exception if service operation failed
	 */

	@Test
	public void testFindBySearchCriteria() throws DAOException,
			ServiceException {
		SearchCriteria sc = new SearchCriteria();
		List<NewsDTO> expectedNewsDTOList = new ArrayList<NewsDTO>();
		when(newsManagementService.findNewsBySearchCriteria(sc, 0, 3))
				.thenReturn(expectedNewsDTOList);
		List<NewsDTO> actualNewsDTOList = newsManagementService
				.findNewsBySearchCriteria(sc, 0, 3);
		verify(newsService).find(sc, 0, 3);
		assertEquals(actualNewsDTOList.size(), 0);
	}

	/**
	 * Test count news.
	 *
	 * @throws DAOException
	 *             the DAO exception if database operation failed
	 * @throws ServiceException
	 *             the service exception if service operation failed
	 */
	@Test
	public void testCountNews() throws DAOException, ServiceException {
		long expectedNewsCount = 10l;
		when(newsManagementService.countNews()).thenReturn(expectedNewsCount);
		long actualNewsCount = newsManagementService.countNews();
		verify(newsService).countNews();
		assertEquals(expectedNewsCount, actualNewsCount);
	}

	/**
	 * Test count news with search criteria.
	 *
	 * @throws DAOException
	 *             the DAO exception if database operation failed
	 * @throws ServiceException
	 *             the service exception if service operation failed
	 */
	@Test
	public void testCountNewsSearch() throws DAOException, ServiceException {
		long expectedNewsCount = 10l;
		SearchCriteria sc = null;
		when(newsManagementService.countNews(sc)).thenReturn(expectedNewsCount);
		long actualNewsCount = newsManagementService.countNews(sc);
		verify(newsService).countNews(sc);
		assertEquals(expectedNewsCount, actualNewsCount);
	}

	/**
	 * Test delete news.
	 *
	 * @throws DAOException
	 *             the DAO exception if database operation failed
	 * @throws ServiceException
	 *             the service exception if service operation failed
	 */
	@Test
	public void testDeleteNews() throws DAOException, ServiceException {
		Long[] newsId = new Long[] { 1L };
		newsManagementService.deleteNews(newsId);
		verify(newsService).delete(newsId);
		verify(newsService).deleteNewsAuthor(newsId);
		verify(newsService).deleteNewsTagByNewsId(newsId);
		verify(commentService).deleteByNewsId(newsId);
	}

	/**
	 * Test delete comment.
	 *
	 * @throws DAOException
	 *             the DAO exception if database operation failed
	 * @throws ServiceException
	 *             the service exception if service operation failed
	 */
	@Test
	public void testDeleteComment() throws DAOException, ServiceException {
		Comment comment = initComment();
		newsManagementService.deleteComment(comment.getCommentId());
		verify(commentService).delete(comment.getCommentId());
	}

	/**
	 * Test save tag.
	 *
	 * @throws DAOException
	 *             the DAO exception if database operation failed
	 * @throws ServiceException
	 *             the service exception if service operation failed
	 */
	@Test
	public void testSaveTag() throws DAOException, ServiceException {
		Tag tag = initTag();
		long expectedId = 1L;
		when(newsManagementService.saveTag(tag)).thenReturn(expectedId);
		long actualId = newsManagementService.saveTag(tag);
		verify(tagService).save(tag);
		assertEquals(expectedId, actualId);
	}

	/**
	 * Test update tag.
	 * 
	 * @throws DAOException
	 *             the DAO exception if database operation failed
	 * @throws ServiceException
	 *             the service exception if service operation failed
	 */
	@Test
	public void testUpdateTag() throws DAOException, ServiceException {
		Tag expectedTag = initTag();
		when(newsManagementService.updateTag(expectedTag)).thenReturn(
				expectedTag);
		Tag actualTag = newsManagementService.updateTag(expectedTag);
		assertEquals(expectedTag, actualTag);
	}

	/**
	 * Test delete tag.
	 * 
	 * @throws DAOException
	 *             the DAO exception if database operation failed
	 * @throws ServiceException
	 *             the service exception if service operation failed
	 */
	@Test
	public void testDeleteTag() throws DAOException, ServiceException {
		Long tagId = 1l;
		newsManagementService.deleteTag(tagId);
		newsService.deleteNewsTagByTagId(tagId);
		verify(tagService).delete(tagId);

	}

	/**
	 * Test find all tag.
	 *
	 * @throws DAOException
	 *             the DAO exception if database operation failed
	 * @throws ServiceException
	 *             the service exception if service operation failed
	 */
	@Test
	public void testFindAllTag() throws DAOException, ServiceException {
		List<Tag> tagList = new ArrayList<Tag>();
		tagList.add(initTag());
		tagList.add(initTag());
		when(newsManagementService.findAllTag()).thenReturn(tagList);
		List<Tag> actualTagList = newsManagementService.findAllTag();
		assertEquals(actualTagList.size(), 2);
	}

	/**
	 * Test save author.
	 *
	 * @throws DAOException
	 *             the DAO exception if database operation failed
	 * @throws ServiceException
	 *             the service exception if service operation failed
	 */
	@Test
	public void testSaveAuthor() throws DAOException, ServiceException {
		Author author = initAuthor();
		long expectedId = 111L;
		when(newsManagementService.saveAuthor(author)).thenReturn(expectedId);
		long actualId = newsManagementService.saveAuthor(author);
		verify(authorService).save(author);
		assertEquals(expectedId, actualId);
	}

	/**
	 * Test update author.
	 *
	 * @throws DAOException
	 *             the DAO exception if database operation failed
	 * @throws ServiceException
	 *             the service exception if service operation failed
	 */
	@Test
	public void testUpdateAuthor() throws DAOException, ServiceException {
		Author expectedAuthor = initAuthor();
		when(newsManagementService.updateAuthor(expectedAuthor)).thenReturn(
				expectedAuthor);
		Author actualAuthor = newsManagementService
				.updateAuthor(expectedAuthor);
		verify(authorService).update(expectedAuthor);
		assertEquals(expectedAuthor, actualAuthor);
	}

	/**
	 * Test find all author.
	 *
	 * @throws DAOException
	 *             the DAO exception if database operation failed
	 * @throws ServiceException
	 *             the service exception if service operation failed
	 */
	@Test
	public void testFindAllAuthor() throws DAOException, ServiceException {
		List<Author> authorList = new ArrayList<Author>();
		authorList.add(initAuthor());
		authorList.add(initAuthor());
		when(newsManagementService.findAllAuthor()).thenReturn(authorList);
		List<Author> actualAuthorList = newsManagementService.findAllAuthor();
		verify(authorService).findAll();
		assertEquals(actualAuthorList.size(), 2);
	}

	/**
	 * Test save comment.
	 *
	 * @throws DAOException
	 *             the DAO exception if database operation failed
	 * @throws ServiceException
	 *             the service exception if service operation failed
	 */
	@Test
	public void testSaveComment() throws DAOException, ServiceException {
		Comment comment = initComment();
		long expectedId = 111L;
		when(newsManagementService.saveComment(comment)).thenReturn(expectedId);
		long actualId = newsManagementService.saveComment(comment);
		verify(commentService).save(comment);
		assertEquals(expectedId, actualId);
	}

	/**
	 * Test make author expired.
	 *
	 * @throws DAOException
	 *             the DAO exception if database operation failed
	 * @throws ServiceException
	 *             the service exception if service operation failed
	 */
	@Test
	public void testMakeAuthorExpired() throws DAOException, ServiceException {
		Author expectedAuthor = initAuthor();
		when(newsManagementService.makeAuthorExpired(expectedAuthor))
				.thenReturn(expectedAuthor);
		Author actualAuthor = newsManagementService
				.makeAuthorExpired(expectedAuthor);
		verify(authorService).update(expectedAuthor);
		assertEquals(expectedAuthor, actualAuthor);
	}

	/**
	 * Inits the author.
	 *
	 * @return the author
	 */
	private Author initAuthor() {
		return new Author(111L, "Jimmy", new Date());
	}

	/**
	 * Inits the news.
	 *
	 * @return the news
	 */
	private News initNews() {
		return new News(0l, "title", "short text", "full text", new Date(),
				new Date());
	}

	/**
	 * Inits the tag.
	 *
	 * @return the tag
	 */
	private Tag initTag() {
		return new Tag(0l, "misc");
	}

	/**
	 * Inits the comment.
	 *
	 * @return the comment
	 */
	private Comment initComment() {
		return new Comment(111L, 1l, "comment", new Date());
	}
}
