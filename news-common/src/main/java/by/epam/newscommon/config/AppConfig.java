package by.epam.newscommon.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@ComponentScan(basePackages = { "by.epam.newscommon" })
@ImportResource({ "classpath:spring-daoconfig.xml" })
public class AppConfig {

}
