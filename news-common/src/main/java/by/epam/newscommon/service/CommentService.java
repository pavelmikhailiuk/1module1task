package by.epam.newscommon.service;

import java.util.List;

import by.epam.newscommon.exception.ServiceException;
import by.epam.newsdao.entity.Comment;

/**
 * The Interface CommentService. Extends interface CRUDService
 */
public interface CommentService extends CRUDService<Comment> {
		
	/**
	 * Find comments by news id.
	 *
	 * @param newsId the news id
	 * @return the list of comment object
	 * @throws ServiceException the service exception if operation failed
	 */
	List<Comment> findByNewsId(Long newsId)throws ServiceException;
	
	/**
	 * Delete comment by news id.
	 *
	 * @param id the array of the news id
	 * @throws ServiceException the service exception
	 */
	public void deleteByNewsId(Long[] id) throws ServiceException;
}
