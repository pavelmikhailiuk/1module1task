package by.epam.newscommon.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import by.epam.newscommon.exception.ServiceException;
import by.epam.newscommon.service.TagService;
import by.epam.newsdao.dao.TagDAO;
import by.epam.newsdao.entity.Tag;
import by.epam.newsdao.exception.DAOException;

/**
 * The Class TagServiceImpl. Implements interface TagService
 */
@Service
public class TagServiceImpl implements TagService {

	/** The tag DAO. */
	@Autowired
	private TagDAO tagDAO;

	/*
	 * (non-Javadoc)
	 * 
	 * @see by.epam.newsmanagement.service.CRUDService#save(java.lang.Object)
	 */
	@Override
	public Long save(Tag tag) throws ServiceException {
		Long id = null;
		try {
			id = tagDAO.save(tag);
		} catch (DAOException e) {
			throw new ServiceException("Error save Tag", e);
		}
		return id;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see by.epam.newsmanagement.service.CRUDService#find(java.lang.Long)
	 */
	@Override
	public Tag find(Long id) throws ServiceException {
		Tag tg = null;
		try {
			tg = tagDAO.find(id);
		} catch (DAOException e) {
			throw new ServiceException("Error find Tag", e);
		}
		return tg;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see by.epam.newsmanagement.service.CRUDService#update(java.lang.Object)
	 */
	@Override
	public Tag update(Tag tag) throws ServiceException {
		try {
			tagDAO.update(tag);
			tag = tagDAO.find(tag.getTagId());
		} catch (DAOException e) {
			throw new ServiceException("Error edit Tag", e);
		}
		return tag;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see by.epam.newsmanagement.service.CRUDService#findAll()
	 */
	@Override
	public List<Tag> findAll() throws ServiceException {
		List<Tag> tagList = null;
		try {
			tagList = tagDAO.findAll();
		} catch (DAOException e) {
			throw new ServiceException("Error find all Tag", e);
		}
		return tagList;
	}

	@Override
	public void delete(Long id) throws ServiceException {
		try {
			tagDAO.delete(id);
		} catch (DAOException e) {
			throw new ServiceException("Error delete Tag", e);
		}

	}

}
