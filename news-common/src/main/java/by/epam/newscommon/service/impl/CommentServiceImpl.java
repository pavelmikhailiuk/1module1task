package by.epam.newscommon.service.impl;

import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import by.epam.newscommon.exception.ServiceException;
import by.epam.newscommon.service.CommentService;
import by.epam.newsdao.dao.CommentDAO;
import by.epam.newsdao.entity.Comment;
import by.epam.newsdao.exception.DAOException;

/**
 * The Class CommentServiceImpl. Implements interface CommentService
 */
@Service
public class CommentServiceImpl implements CommentService {

	/** The comment DAO. */
	@Autowired
	private CommentDAO commentDAO;

	/*
	 * (non-Javadoc)
	 * 
	 * @see by.epam.newsmanagement.service.CRUDService#save(java.lang.Object)
	 */
	@Override
	public Long save(Comment comment) throws ServiceException {
		Long id = null;
		try {
			id = commentDAO.save(comment);
		} catch (DAOException e) {
			throw new ServiceException("Error save Comment", e);
		}
		return id;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see by.epam.newsmanagement.service.CRUDService#find(java.lang.Long)
	 */
	@Override
	public Comment find(Long id) throws ServiceException {
		Comment comm = null;
		try {
			comm = commentDAO.find(id);
		} catch (DAOException e) {
			throw new ServiceException("Error find Comment", e);
		}
		return comm;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see by.epam.newsmanagement.service.CRUDService#update(java.lang.Object)
	 */
	@Override
	public Comment update(Comment comment) throws ServiceException {
		try {
			commentDAO.update(comment);
			comment = commentDAO.find(comment.getCommentId());
		} catch (DAOException e) {
			throw new ServiceException("Error edit Comment", e);
		}
		return comment;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see by.epam.newsmanagement.service.CommentService#delete(java.lang.Long)
	 */
	@Override
	public void delete(Long id) throws ServiceException {
		try {
			commentDAO.delete(id);
		} catch (DAOException e) {
			throw new ServiceException("Error delete Comment", e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see by.epam.newsmanagement.service.CRUDService#findAll()
	 */
	@Override
	public List<Comment> findAll() throws ServiceException {
		List<Comment> commentList = null;
		try {
			commentList = commentDAO.findAll();
		} catch (DAOException e) {
			throw new ServiceException("Error find all Comment", e);
		}
		return commentList;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * by.epam.newsmanagement.service.CommentService#findByNewsId(java.lang.
	 * Long)
	 */
	@Override
	public List<Comment> findByNewsId(Long newsId) throws ServiceException {
		List<Comment> commentList = null;
		try {
			commentList = commentDAO.findAll();
			Iterator<Comment> commentListIterator = commentList.iterator();
			while (commentListIterator.hasNext()) {
				Comment comment = commentListIterator.next();
				if (comment.getNewsId() != newsId) {
					commentListIterator.remove();
				}
			}
		} catch (DAOException e) {
			throw new ServiceException("Error find all Comment by News id", e);
		}
		return commentList;
	}

	@Override
	public void deleteByNewsId(Long[] id) throws ServiceException {
		try {
			commentDAO.deleteByNewsId(id);
		} catch (DAOException e) {
			throw new ServiceException("Error delete Comment", e);
		}

	}

}
