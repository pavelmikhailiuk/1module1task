package by.epam.newscommon.service.impl;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import by.epam.newscommon.exception.ServiceException;
import by.epam.newscommon.service.*;
import by.epam.newsdao.entity.*;

/**
 * The Class NewsManagementServiceImpl. Implements interface
 * NewsManagementService
 */
@Service
public class NewsManagementServiceImpl implements NewsManagementService {

	/** The news service. */
	@Autowired
	private NewsService newsService;

	/** The comment service. */
	@Autowired
	private CommentService commentService;

	/** The tag service. */
	@Autowired
	private TagService tagService;

	/** The author service. */
	@Autowired
	private AuthorService authorService;

	/**
	 * Adds the news.
	 *
	 * @param newsDTO
	 *            the news data transfer object
	 * @return the long id
	 * @throws ServiceException
	 *             the service exception if operation failed
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * by.epam.newsmanagement.service.NewsManagementService#addNews(by.epam.
	 * newsmanagement.entity.NewsDTO)
	 */
	@Override
	@Transactional(rollbackFor = ServiceException.class)
	public Long addNews(NewsDTO newsDTO) throws ServiceException {
		News news = newsDTO.getNews();
		long newsId = newsService.save(news);
		List<Tag> tags = newsDTO.getTags();
		if (tags != null) {
			for (Tag tag : tags) {
				long tagId = tag.getTagId();
				newsService.saveNewsTag(newsId, tagId);
			}
		}
		Author author = newsDTO.getAuthor();
		if (author != null) {
			long authorId = author.getAuthorId();
			newsService.saveNewsAuthor(newsId, authorId);
		}
		return newsId;
	}

	/**
	 * Find news by search criteria.
	 *
	 * @param searchCriteria
	 *            the search criteria
	 * @param offset
	 *            the offset
	 * @param newsAtPage
	 *            the news at page
	 * @return the list of news DTO object
	 * @throws ServiceException
	 *             the service exception if operation failed
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see by.epam.newsmanagement.service.NewsManagementService#
	 * findNewsBySearchCriteria (by.epam.newsmanagement.entity.SearchCriteria)
	 */
	@Override
	public List<NewsDTO> findNewsBySearchCriteria(SearchCriteria searchCriteria, int offset, int newsAtPage)
			throws ServiceException {
		List<News> newsList = newsService.find(searchCriteria, offset, newsAtPage);
		return initListNewsDTO(newsList);
	}

	/**
	 * Edits the news.
	 *
	 * @param newsDTO
	 *            the news DTO object
	 * @return the news DTO object
	 * @throws ServiceException
	 *             the service exception if operation failed
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * by.epam.newsmanagement.service.NewsManagementService#editNews(by.epam
	 * .newsmanagement.entity.NewsDTO)
	 */
	@Override
	@Transactional(rollbackFor = ServiceException.class)
	public NewsDTO editNews(NewsDTO newsDTO) throws ServiceException {
		News news = newsDTO.getNews();
		long newsId = news.getNewsId();
		Long[] newsID = new Long[] { newsId };
		news = newsService.update(news);
		newsDTO.setNews(news);
		List<Tag> tagList = newsDTO.getTags();
		if (tagList != null) {
			newsService.deleteNewsTagByNewsId(newsID);
			for (Tag tag : tagList) {
				long tagId = tag.getTagId();
				newsService.saveNewsTag(newsId, tagId);
			}
		}
		Author author = newsDTO.getAuthor();
		if (author != null) {
			long authorId = author.getAuthorId();
			newsService.updateNewsAuthor(newsId, authorId);
		}
		return newsDTO;
	}

	/**
	 * Delete news.
	 *
	 * @param newsDTO
	 *            the news DTO object
	 * @throws ServiceException
	 *             the service exception if operation failed
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * by.epam.newsmanagement.service.NewsManagementService#deleteNews(by.epam
	 * .newsmanagement.entity.NewsDTO)
	 */
	@Override
	@Transactional(rollbackFor = ServiceException.class)
	public void deleteNews(Long[] newsId) throws ServiceException {
		commentService.deleteByNewsId(newsId);
		newsService.deleteNewsAuthor(newsId);
		newsService.deleteNewsTagByNewsId(newsId);
		newsService.delete(newsId);
	}

	/**
	 * Find all news.
	 *
	 * @param offset
	 *            the offset
	 * @param newsAtPage
	 *            the news at page
	 * @return the list of news DTO object
	 * @throws ServiceException
	 *             the service exception if operation failed
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see by.epam.newsmanagement.service.NewsManagementService#findAllNews()
	 */
	@Override
	public List<NewsDTO> findAllNews(int offset, int newsAtPage) throws ServiceException {
		List<News> newsList = newsService.findAll(offset, newsAtPage);
		return initListNewsDTO(newsList);
	}

	/**
	 * Save author.
	 *
	 * @param author
	 *            the author object
	 * @return the long id
	 * @throws ServiceException
	 *             the service exception if operation failed
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * by.epam.newsmanagement.service.NewsManagementService#saveAuthor(by.epam
	 * .newsmanagement.entity.Author)
	 */
	@Override
	public Long saveAuthor(Author author) throws ServiceException {
		return authorService.save(author);
	}

	/**
	 * Save tag.
	 *
	 * @param tag
	 *            the tag object
	 * @return the long id
	 * @throws ServiceException
	 *             the service exception if operation failed
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * by.epam.newsmanagement.service.NewsManagementService#saveTag(by.epam.
	 * newsmanagement.entity.Tag)
	 */
	@Override
	public Long saveTag(Tag tag) throws ServiceException {
		return tagService.save(tag);
	}

	/**
	 * Save comment.
	 *
	 * @param comment
	 *            the comment
	 * @return the long
	 * @throws ServiceException
	 *             the service exception if operation failed
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * by.epam.newsmanagement.service.NewsManagementService#saveComment(by.epam
	 * .newsmanagement.entity.Comment)
	 */
	@Override
	public Long saveComment(Comment comment) throws ServiceException {
		return commentService.save(comment);
	}

	/**
	 * Delete comment.
	 *
	 * @param id
	 *            the id
	 * @throws ServiceException
	 *             the service exception if operation failed
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * by.epam.newsmanagement.service.NewsManagementService#deleteComment(java
	 * .lang.Long)
	 */
	@Override
	public void deleteComment(Long id) throws ServiceException {
		commentService.delete(id);
	}

	/**
	 * Count news.
	 *
	 * @return the long number of news
	 * @throws ServiceException
	 *             the service exception if operation failed
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see by.epam.newsmanagement.service.NewsManagementService#countNews()
	 */
	@Override
	public Long countNews() throws ServiceException {
		return newsService.countNews();
	}

	/**
	 * Make author expired.
	 *
	 * @param author
	 *            the author object
	 * @return the updated author object
	 * @throws ServiceException
	 *             the service exception if operation failed
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * by.epam.newsmanagement.service.NewsManagementService#makeAuthorExpired
	 * (by.epam.newsmanagement.entity.Author)
	 */
	@Override
	public Author makeAuthorExpired(Author author) throws ServiceException {
		author.setExpired(new Date());
		return authorService.update(author);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * by.epam.newsmanagement.service.NewsManagementService#countNews(by.epam
	 * .newsmanagement.entity.SearchCriteria)
	 */
	@Override
	public Long countNews(SearchCriteria sc) throws ServiceException {
		return newsService.countNews(sc);
	}

	/**
	 * Find news.
	 *
	 * @param newsId
	 *            the news id
	 * @return the news DTO
	 * @throws ServiceException
	 *             the service exception if operation failed
	 */
	@Override
	public NewsDTO findNews(Long newsId) throws ServiceException {
		News news = newsService.find(newsId);
		return initNewsDTO(news);
	}

	@Override
	public List<Author> findAllAuthor() throws ServiceException {
		return authorService.findAll();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see by.epam.newsmanagement.service.NewsManagementService#findAllTag()
	 */
	@Override
	public List<Tag> findAllTag() throws ServiceException {
		return tagService.findAll();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * by.epam.newsmanagement.service.NewsManagementService#updateAuthor(by.
	 * epam.newsmanagement.entity.Author)
	 */
	@Override
	public Author updateAuthor(Author author) throws ServiceException {
		return authorService.update(author);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * by.epam.newsmanagement.service.NewsManagementService#updateTag(by.epam
	 * .newsmanagement.entity.Tag)
	 */
	@Override
	public Tag updateTag(Tag tag) throws ServiceException {
		return tagService.update(tag);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * by.epam.newsmanagement.service.NewsManagementService#deleteTag(java.lang
	 * .Long)
	 */
	@Override
	@Transactional(rollbackFor = ServiceException.class)
	public void deleteTag(Long tagId) throws ServiceException {
		newsService.deleteNewsTagByTagId(tagId);
		tagService.delete(tagId);
	}

	/**
	 * Inits the list news DTO.
	 *
	 * @param newsList
	 *            the list of news object
	 * @return the list of news DTO object
	 * @throws ServiceException
	 *             the service exception if operation failed
	 */
	private List<NewsDTO> initListNewsDTO(List<News> newsList) throws ServiceException {
		List<NewsDTO> newsDTOList = new ArrayList<NewsDTO>();
		if (newsList.size() > 0) {
			for (News news : newsList) {
				newsDTOList.add(initNewsDTO(news));
			}
		}
		return newsDTOList;
	}

	/**
	 * Inits the news DTO.
	 *
	 * @param news
	 *            the news
	 * @return the news DTO
	 */
	private NewsDTO initNewsDTO(News news) throws ServiceException {
		NewsDTO newsDTO = null;
		if (news != null) {
			long newsId = news.getNewsId();
			long authorId = newsService.findAuthorIdByNewsId(newsId);
			Author author = authorService.find(authorId);
			List<Comment> commentList = commentService.findByNewsId(newsId);
			List<Tag> tagList = new ArrayList<>();
			List<Long> tagIdList = newsService.findTagIdByNewsId(newsId);
			for (Long tagId : tagIdList) {
				tagList.add(tagService.find(tagId));
			}
			newsDTO = new NewsDTO(news, author, tagList, commentList);
		}
		return newsDTO;
	}
}
