package by.epam.newscommon.service;

import by.epam.newsdao.entity.Tag;

/**
 * The Interface TagService. Extends interface CRUDService
 */
public interface TagService extends CRUDService<Tag> {

}
