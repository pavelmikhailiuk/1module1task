package by.epam.newscommon.service;

import java.util.List;

import by.epam.newscommon.exception.ServiceException;
import by.epam.newsdao.entity.News;
import by.epam.newsdao.entity.SearchCriteria;

/**
 * The Interface NewsService. Extends interface CRUDService
 */
public interface NewsService extends CRUDService<News> {

	/**
	 * Find.
	 *
	 * @param searchCriteria
	 *            the search criteria
	 * @return the list of news object
	 * @throws ServiceException
	 *             the service exception if operation failed
	 */
	List<News> find(SearchCriteria searchCriteria, int offset, int newsAtPage)
			throws ServiceException;

	/**
	 * Find all for pagination.
	 *
	 * @param offset
	 *            the offset
	 * @param newsAtPage
	 *            the news quantity at page
	 * @return the list of the generic type
	 * @throws ServiceException
	 *             the service exception if operation failed
	 */
	List<News> findAll(int offset, int newsAtPage) throws ServiceException;

	/**
	 * Count news.
	 *
	 * @return the long number of news
	 * @throws ServiceException
	 *             the service exception if operation failed
	 */
	Long countNews() throws ServiceException;

	/**
	 * Count found by search criteria news.
	 *
	 * @return the long number of news
	 * @throws ServiceException
	 *             the service exception if operation failed
	 */
	Long countNews(SearchCriteria sc) throws ServiceException;

	/**
	 * Save news author.
	 *
	 * @param newsId
	 *            the news id
	 * @param authorId
	 *            the author id
	 * @throws ServiceException
	 *             the service exception if operation failed
	 */
	void saveNewsAuthor(Long newsId, Long authorId) throws ServiceException;

	/**
	 * Update news author.
	 *
	 * @param newsId
	 *            the news id
	 * @param authorId
	 *            the author id
	 * @throws ServiceException
	 *             the service exception if operation failed
	 */
	void updateNewsAuthor(Long newsId, Long authorId) throws ServiceException;

	/**
	 * Delete news author.
	 *
	 * @param newsId
	 *            the news id
	 * @throws ServiceException
	 *             the service exception if operation failed
	 */
	void deleteNewsAuthor(Long[] newsId) throws ServiceException;

	/**
	 * Save news tag.
	 *
	 * @param newsId
	 *            the news id
	 * @param tagId
	 *            the tag id
	 * @throws ServiceException
	 *             the service exception if operation failed
	 */
	void saveNewsTag(Long newsId, Long tagId) throws ServiceException;

	/**
	 * Update news tag.
	 *
	 * @param newsId
	 *            the news id
	 * @param tagId
	 *            the tag id
	 * @throws ServiceException
	 *             the service exception if operation failed
	 */
	void updateNewsTag(Long newsId, Long tagId) throws ServiceException;

	
	/**
	 * Delete news tag by news id.
	 *
	 * @param newsId the news id
	 * @throws ServiceException the service exception if operation failed
	 */
	void deleteNewsTagByNewsId(Long[] newsId) throws ServiceException;

	
	/**
	 * Delete news tag by tag id.
	 *
	 * @param tagId the tag id
	 * @throws ServiceException the service exception if operation failed
	 */
	void deleteNewsTagByTagId(Long tagId) throws ServiceException;
	
	/**
	 * Find author id by news id.
	 *
	 * @param newsId
	 *            the news id
	 * @return the long author id
	 * @throws ServiceException
	 *             the service exception if operation failed
	 */
	Long findAuthorIdByNewsId(Long newsId) throws ServiceException;

	/**
	 * Find tag id by news id.
	 *
	 * @param newsId
	 *            the news id
	 * @return the list of long tag id
	 * @throws ServiceException
	 *             the service exception if operation failed
	 */
	List<Long> findTagIdByNewsId(Long newsId) throws ServiceException;
	
	/**
	 * Delete.
	 *
	 * @param id the array of news id
	 * @throws ServiceException the service exception if operation failed
	 */
	void delete(Long[] id)throws ServiceException;
}
