package by.epam.newscommon.service;

import java.util.List;

import by.epam.newscommon.exception.ServiceException;
import by.epam.newsdao.entity.*;

/**
 * The Interface NewsManagementService.
 */
public interface NewsManagementService {
	
	/**
	 * Adds the news.
	 *
	 * @param newsDTO the news data transfer object
	 * @return the long id
	 * @throws ServiceException the service exception if operation failed
	 */
	Long addNews(NewsDTO newsDTO) throws ServiceException;

	/**
	 * Find news.
	 *
	 * @param newsId the news id
	 * @return the newsDTO the news data transfer object
	 * @throws ServiceException the service exception
	 */
	NewsDTO findNews(Long newsId) throws ServiceException;
	
	/**
	 * Find news by search criteria.
	 *
	 * @param searchCriteria the search criteria
	 * @return the list of data transfer object
	 * @throws ServiceException the service exception if operation failed
	 */
	List<NewsDTO> findNewsBySearchCriteria(SearchCriteria searchCriteria,int offset, int newsAtPage)
			throws ServiceException;

	/**
	 * Edits the news.
	 *
	 * @param newsDTO the news data transfer object
	 * @return the news data transfer object
	 * @throws ServiceException the service exception if operation failed
	 */
	NewsDTO editNews(NewsDTO newsDTO) throws ServiceException;

	/**
	 * Delete news.
	 *
	 * @param newsDTO the news data transfer object
	 * @throws ServiceException the service exception if operation failed
	 */
	void deleteNews(Long[] newsId) throws ServiceException;

	/**
	 * Find all news.
	 *
	 * @return the list of data transfer object
	 * @throws ServiceException the service exception if operation failed
	 */
	List<NewsDTO> findAllNews(int offset, int newsAtPage) throws ServiceException;

	/**
	 * Save author.
	 *
	 * @param author the author
	 * @return the long id
	 * @throws ServiceException the service exception if operation failed
	 */
	Long saveAuthor(Author author) throws ServiceException;

	/**
	 * Save tag.
	 *
	 * @param tag the tag
	 * @return the long id
	 * @throws ServiceException the service exception if operation failed
	 */
	Long saveTag(Tag tag) throws ServiceException;

	/**
	 * Save comment.
	 *
	 * @param comment the comment
	 * @return the long id
	 * @throws ServiceException the service exception if operation failed
	 */
	Long saveComment(Comment comment) throws ServiceException;

	/**
	 * Delete comment.
	 *
	 * @param id the id
	 * @throws ServiceException the service exception if operation failed
	 */
	void deleteComment(Long id) throws ServiceException;

	/**
	 * Count news.
	 *
	 * @return the long number of news
	 * @throws ServiceException the service exception if operation failed 
	 */
	Long countNews() throws ServiceException;
	
	/**
	 * Count news found by search criteria.
	 *
	 * @return the long number of news
	 * @throws ServiceException the service exception if operation failed 
	 */
	Long countNews(SearchCriteria sc) throws ServiceException;

	/**
	 * Make author expired.
	 *
	 * @param author the author object
	 * @return the author object
	 * @throws ServiceException the service exception if operation failed
	 */
	Author makeAuthorExpired(Author author)throws ServiceException;
	
	/**
	 * Find all author.
	 *
	 * @return the list of author object
	 * @throws ServiceException the service exception if operation failed
	 */
	List<Author> findAllAuthor()throws ServiceException;
	
	/**
	 * Find all tag.
	 *
	 * @return the list of tag object
	 * @throws ServiceException the service exception if operation failed
	 */
	List<Tag> findAllTag()throws ServiceException;
	
	/**
	 * Update author.
	 *
	 * @param author the author object
	 * @return the author
	 * @throws ServiceException the service exception if operation failed
	 */
	Author updateAuthor(Author author)throws ServiceException;
	
	/**
	 * Update tag.
	 *
	 * @param tag the tag object
	 * @return the tag
	 * @throws ServiceException the service exception if operation failed
	 */
	Tag updateTag(Tag tag)throws ServiceException;
	
	
	/**
	 * Delete tag.
	 *
	 * @param newsId the news id
	 * @param tagId the tag id
	 * @throws ServiceException the service exception if operation failed
	 */
	void deleteTag(Long tagId)throws ServiceException;
	
}
