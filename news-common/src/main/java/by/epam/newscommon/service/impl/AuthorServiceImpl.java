package by.epam.newscommon.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import by.epam.newscommon.exception.ServiceException;
import by.epam.newscommon.service.AuthorService;
import by.epam.newsdao.dao.AuthorDAO;
import by.epam.newsdao.entity.Author;
import by.epam.newsdao.exception.DAOException;

/**
 * The Class AuthorServiceImpl. Implements interface AuthorService
 */
@Service
public class AuthorServiceImpl implements AuthorService {
	
	/** The author DAO. */
	@Autowired
	private AuthorDAO authorDAO;

	/*
	 * (non-Javadoc)
	 * 
	 * @see by.epam.newsmanagement.service.CRUDService#save(java.lang.Object)
	 */
	@Override
	public Long save(Author author) throws ServiceException {
		Long id = null;
		try {
			id = authorDAO.save(author);
		} catch (DAOException e) {
						throw new ServiceException("Error save Author",e);
		}
		return id;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see by.epam.newsmanagement.service.CRUDService#find(java.lang.Long)
	 */
	@Override
	public Author find(Long id) throws ServiceException {
		Author auth = null;
		try {
			auth = authorDAO.find(id);
		} catch (DAOException e) {
						throw new ServiceException("Error find Author",e);
		}
		return auth;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see by.epam.newsmanagement.service.CRUDService#update(java.lang.Object)
	 */
	@Override
	public Author update(Author author) throws ServiceException {
		try {
			authorDAO.update(author);
			author = authorDAO.find(author.getAuthorId());
		} catch (DAOException e) {
						throw new ServiceException("Error update Author",e);
		}
		return author;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see by.epam.newsmanagement.service.CRUDService#findAll()
	 */
	@Override
	public List<Author> findAll() throws ServiceException {
		List<Author> authorList = null;
		try {
			authorList = authorDAO.findAll();
		} catch (DAOException e) {
						throw new ServiceException("Error find all Author",e);
		}
		return authorList;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see by.epam.newsmanagement.service.CRUDService#delete(java.lang.Long)
	 */
	@Override
	public void delete(Long id) throws ServiceException {
		throw new UnsupportedOperationException("Author can't be deleted");
	}
}
