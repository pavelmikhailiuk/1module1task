package by.epam.newscommon.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import by.epam.newscommon.exception.ServiceException;
import by.epam.newscommon.service.NewsService;
import by.epam.newsdao.dao.NewsDAO;
import by.epam.newsdao.entity.News;
import by.epam.newsdao.entity.SearchCriteria;
import by.epam.newsdao.exception.DAOException;

/**
 * The Class NewsServiceImpl. Implements interface NewsService
 */
@Service
public class NewsServiceImpl implements NewsService {

	/** The news DAO. */
	@Autowired
	private NewsDAO newsDAO;

	/*
	 * (non-Javadoc)
	 * 
	 * @see by.epam.newsmanagement.service.CRUDService#save(java.lang.Object)
	 */
	@Override
	public Long save(News news) throws ServiceException {
		Long newsId = null;
		try {
			newsId = newsDAO.save(news);
		} catch (DAOException e) {

			throw new ServiceException("Error save News", e);
		}
		return newsId;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see by.epam.newsmanagement.service.CRUDService#find(java.lang.Long)
	 */
	@Override
	public News find(Long newsId) throws ServiceException {
		News news = null;
		try {
			news = newsDAO.find(newsId);
		} catch (DAOException e) {
			throw new ServiceException("Error find News", e);
		}
		return news;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * by.epam.newsmanagement.service.NewsService#find(by.epam.newsmanagement.
	 * entity.SearchCriteria)
	 */
	@Override
	public List<News> find(SearchCriteria searchCriteria, int offset, int newsAtPage) throws ServiceException {
		List<News> newsList = null;
		try {
			newsList = newsDAO.findBySearchCriteria(searchCriteria, offset, newsAtPage);
		} catch (DAOException e) {
			throw new ServiceException("Error find News by search criteria", e);
		}
		return newsList;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see by.epam.newsmanagement.service.CRUDService#update(java.lang.Object)
	 */
	@Override
	public News update(News news) throws ServiceException {
		try {
			newsDAO.update(news);
			news = newsDAO.find(news.getNewsId());
		} catch (DAOException e) {
			throw new ServiceException("Error update News", e);
		}
		return news;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see by.epam.newsmanagement.service.NewsService#delete(java.lang.Long)
	 */
	@Override
	public void delete(Long newsId) throws ServiceException {
		try {
			newsDAO.delete(newsId);
		} catch (DAOException e) {
			throw new ServiceException("Error delete News", e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see by.epam.newsmanagement.service.CRUDService#findAll()
	 */
	@Override
	public List<News> findAll(int offset, int newsAtPage) throws ServiceException {
		List<News> newsList = null;
		try {
			newsList = newsDAO.findAll(offset, newsAtPage);
		} catch (DAOException e) {
			throw new ServiceException("Error find all News", e);
		}
		return newsList;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see by.epam.newsmanagement.service.NewsService#countNews()
	 */
	@Override
	public Long countNews() throws ServiceException {
		long newsCount = 0;
		try {
			newsCount = newsDAO.newsCount();
		} catch (DAOException e) {
			throw new ServiceException("Error count News", e);
		}
		return newsCount;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * by.epam.newsmanagement.service.NewsService#saveNewsAuthor(java.lang.Long,
	 * java.lang.Long)
	 */
	@Override
	public void saveNewsAuthor(Long newsId, Long authorId) throws ServiceException {
		try {
			newsDAO.saveNewsAuthor(newsId, authorId);
		} catch (DAOException e) {
			throw new ServiceException("Error save NewsAuthor", e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * by.epam.newsmanagement.service.NewsService#updateNewsAuthor(java.lang.
	 * Long, java.lang.Long)
	 */
	@Override
	public void updateNewsAuthor(Long newsId, Long authorId) throws ServiceException {
		try {
			newsDAO.updateNewsAuthor(newsId, authorId);
		} catch (DAOException e) {
			throw new ServiceException("Error update NewsAuthor", e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * by.epam.newsmanagement.service.NewsService#deleteNewsAuthor(java.lang.
	 * Long)
	 */
	@Override
	public void deleteNewsAuthor(Long[] newsId) throws ServiceException {
		try {
			newsDAO.deleteNewsAuthor(newsId);
		} catch (DAOException e) {
			throw new ServiceException("Error delete NewsAuthor", e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * by.epam.newsmanagement.service.NewsService#saveNewsTag(java.lang.Long,
	 * java.lang.Long)
	 */
	@Override
	public void saveNewsTag(Long newsId, Long tagId) throws ServiceException {
		try {
			newsDAO.saveNewsTag(newsId, tagId);
		} catch (DAOException e) {
			throw new ServiceException("Error save NewsTag", e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * by.epam.newsmanagement.service.NewsService#updateNewsTag(java.lang.Long,
	 * java.lang.Long)
	 */
	@Override
	public void updateNewsTag(Long newsId, Long tagId) throws ServiceException {
		try {
			newsDAO.updateNewsTag(newsId, tagId);
		} catch (DAOException e) {
			throw new ServiceException("Error update NewsTag", e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * by.epam.newsmanagement.service.NewsService#deleteNewsTagByNewsId(java.
	 * lang.Long)
	 */
	@Override
	public void deleteNewsTagByNewsId(Long[] newsId) throws ServiceException {
		try {
			newsDAO.deleteNewsTagByNewsId(newsId);
		} catch (DAOException e) {
			throw new ServiceException("Error delete NewsTag by News Id", e);
		}
	}

	@Override
	public void deleteNewsTagByTagId(Long tagId) throws ServiceException {
		try {
			newsDAO.deleteNewsTagByTagId(tagId);
		} catch (DAOException e) {
			throw new ServiceException("Error delete NewsTag by Tag Id", e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * by.epam.newsmanagement.service.NewsService#findAuthorIdByNewsId(java.lang
	 * .Long)
	 */
	@Override
	public Long findAuthorIdByNewsId(Long newsId) throws ServiceException {
		long authorId = 0;
		try {
			authorId = newsDAO.findAuthorIdByNewsId(newsId);
		} catch (DAOException e) {
			throw new ServiceException("Error find Author Id By News Id", e);
		}
		return authorId;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * by.epam.newsmanagement.service.NewsService#findTagIdByNewsId(java.lang.
	 * Long)
	 */
	@Override
	public List<Long> findTagIdByNewsId(Long newsId) throws ServiceException {
		List<Long> tagIdList = new ArrayList<>();
		try {
			tagIdList = newsDAO.findTagIdByNewsId(newsId);
		} catch (DAOException e) {
			throw new ServiceException("Error find tag id by news id", e);
		}
		return tagIdList;
	}

	@Override
	public List<News> findAll() throws ServiceException {
		List<News> newsList = null;
		try {
			newsList = newsDAO.findAll();
		} catch (DAOException e) {
			throw new ServiceException("Error find all News", e);
		}
		return newsList;
	}

	@Override
	public Long countNews(SearchCriteria sc) throws ServiceException {
		long newsCount = 0;
		try {
			newsCount = newsDAO.newsCount(sc);
		} catch (DAOException e) {
			throw new ServiceException("Error count News by search criteria", e);
		}
		return newsCount;
	}

	@Override
	public void delete(Long[] newsid) throws ServiceException {
		try {
			newsDAO.delete(newsid);
		} catch (DAOException e) {
			throw new ServiceException("Error delete News", e);
		}
	}
}
