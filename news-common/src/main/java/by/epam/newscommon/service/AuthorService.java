package by.epam.newscommon.service;

import by.epam.newsdao.entity.Author;

/**
 * The Interface AuthorService. Extends interface CRUDService
 */
public interface AuthorService extends CRUDService<Author> {

}
